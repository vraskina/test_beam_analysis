#pragma once
#include <array>
#include "Analysis.h"
#include "Plot.h"
#include "TApplication.h"
#include "TRootCanvas.h"
#include "TStyle.h"
#include "../atlasstyle/AtlasStyle.C"
#include "../atlasstyle/AtlasUtils.h"


#include <fstream>

using namespace std;

std::array<TH2F *, N_OF_DEVICES> hist_XY;
std::array<TGraph *, N_OF_DEVICES> grEffCh;
std::array<RooPlot *, N_OF_DEVICES> xframe;
std::array<int, N_OF_DEVICES> channels;
std::array<float, N_OF_DEVICES> MPV;
std::array<float, N_OF_DEVICES> efficiency;
std::array<float, 6> efficiency_vs_charge;

//--------------  Things to change every batch -----------------//

// std::array<TString, N_OF_DEVICES> sensor_name{{"FBK-UFSD3.2-W19", "IHEP-IMEv2-W7Q2"}};
// std::array<TString, N_OF_DEVICES> sensor_name{{"FBK-UFSD3.2-W19","FBK-UFSD3.2-W10"}};
// std::array<TString, N_OF_DEVICES> sensor_name{{"IHEP-IMEv2-W7Q2","USTC-IMEv2.1-W17"}};
std::array<TString, N_OF_DEVICES> sensor_name{{"USTC-IMEv2.1-W19","LGA39"}};

// std::array<TString, N_OF_DEVICES> irradiation{{"1.5x10^{15}n_{eq}cm^{-2}", "1.5x10^{15}n_{eq}cm^{-2}"}};
std::array<TString, N_OF_DEVICES> irradiation{{"2.5x10^{15}n_{eq}cm^{-2}", "0x10^{15}n_{eq}cm^{-2}"}};

std::array<TString, N_OF_DEVICES> BV {{"520", "90"}};

TString path = "/eos/home-v/vraskina/Analysis_Test_Beam/Feb2022new/batch7xx/";

TString batch = "705/";

TString filename = path + batch + "tree_February2022_705.root"; 
