#include "run_analysis.h"

using namespace ROOT;

int main() {
  SetAtlasStyle();
  Analysis obj;
  obj.CutTimeMin = {0, 0};
  obj.pulseheightSiPM_cut = 5;
  obj.MinCh = {0, 0};
  obj.mean_charge_guess = {10, 10};
//   bool test = 0; //This thing was for a test in the first commit, no need to use it

  if (not test)
  {
    obj.Run(filename, channels, hist_XY, grEffCh, efficiency, MPV, xframe);
    Plot plt;

    for (int channel : channels) {
      plt.save_fig_efficiency(hist_XY.at(channel), channel, 
            path, sensor_name.at(channel), 
            irradiation.at(channel), batch);
      plt.save_fig_charge(xframe.at(channel), channel, 
            path, sensor_name.at(channel), 
            irradiation.at(channel), batch, MPV.at(channel));
      plt.save_fig_eff_vs_charge(grEffCh.at(channel), channel, 
            path, sensor_name.at(channel), 
            irradiation.at(channel), batch);
      plt.save_fig_eff_projection(hist_XY.at(channel),channel, path, 
            sensor_name.at(channel), irradiation.at(channel), batch, efficiency.at(channel));
      obj.write_to_file(path, sensor_name.at(channel), BV.at(channel), 
            efficiency.at(channel), MPV.at(channel));
      obj.write_parameters(path, sensor_name.at(channel), batch, 
            obj.CutTimeMin.at(channel), obj.MinCh.at(channel), 
            obj.mean_charge_guess.at(channel) );
    }
  }

//   if (test)
//   { 
//     TApplication app("app", nullptr, nullptr);
//     std::array<TCanvas *, N_OF_DEVICES> c_eff;
//     c_eff[0] =
//         new TCanvas(Form("c_eff%d", channels), Form("c_eff%d", channels), 900, 900);
//     c_eff[0]->cd();
//     hist_XY.at(0)->Draw("colz");
//     c_eff[0]->Modified();// This is for the canvas after Draw() to appear on screen
//     c_eff[0]->Update(); // This is for the canvas after Draw() to appear on screen
//     TRootCanvas *rc = (TRootCanvas *)c_eff[0]->GetCanvasImp();
    
//     // This is to stop program after  close Canvas
//     rc->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");
//     app.Run();
//   }
  
 return 0;
}