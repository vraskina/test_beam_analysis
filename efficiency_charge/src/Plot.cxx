#include "Plot.h"

void Plot::save_fig_efficiency(TH2F *hist_XY, int channel, 
            TString path, TString sensor_name, 
            TString irradiation, 
            TString batch){
    TCanvas *c_eff = new TCanvas(Form("c_eff%d", channel), Form("c_eff%d", channel), 600, 600);
    TLatex *t = new TLatex();
    t->SetNDC();
    t->SetTextFont(72);
    t->SetTextColor(1);
    t->SetTextSize(0.05);
    t->SetTextAlign(12);
    hist_XY->SetTitle("ATLAS HGTD Preliminary");
    hist_XY->GetXaxis()->SetTitle("X [mm]");
    hist_XY->GetYaxis()->SetTitle("Y [mm]");
    hist_XY->GetYaxis()->SetTitleOffset(1.2);
    hist_XY->SetStats(0);
    c_eff->cd();
    hist_XY->Draw("colz");
    t->DrawLatex(0.133,0.93,"ATLAS HGTD");
    t->SetTextFont(42);
    t->SetTextSize(0.05);
    t->DrawLatex(0.43,0.925,"Preliminary Test Beam");
    t->SetTextFont(62);
    t->SetTextColor(12);
    t->SetTextSize(0.04);

    t->DrawLatex(0.5, 0.84, sensor_name);
    t->DrawLatex(0.2, 0.2, "#phi = " + irradiation);
    t->DrawLatex(0.2, 0.84, "Efficiency (%)");
    std::cout << "pwd = " << gSystem->pwd() << std::endl; c_eff->Modified(); c_eff->Update();
    c_eff->Print(path + batch + "efficiency" + sensor_name + ".png");
    c_eff->Print(path + batch + "efficiency" + sensor_name + ".C");
};

void Plot::save_fig_charge(RooPlot * xframe, int channel, 
            TString path, TString sensor_name, 
            TString irradiation, 
            TString batch, float MPV){
    TLatex *t = new TLatex();
    t->SetNDC();
    TCanvas *c_charge =
        new TCanvas(Form("c_charge%d", channel), Form("c_charge%d", channel), 600, 600);
    c_charge->cd();
    xframe->GetYaxis()->SetTitleOffset(1.6);
    xframe->Draw();
    t->SetTextFont(72);
    t->SetTextColor(1);
    t->SetTextSize(0.05);
    t->SetTextAlign(12);
    t->DrawLatex(0.133,0.93,"ATLAS HGTD");
    t->SetTextFont(42);
    t->SetTextSize(0.05);
    t->DrawLatex(0.43,0.925,"Preliminary Test Beam");
    t->SetTextFont(62);
    t->SetTextColor(12);
    t->SetTextSize(0.04);
    t->DrawLatex(0.5, 0.8, sensor_name);
    t->DrawLatex(0.5, 0.75, "#phi =" + irradiation);
    t->DrawLatex(0.5, 0.5, Form("MPV = %.2f +/- 0.03", MPV));
    c_charge->Print(path + batch + "charge_" + sensor_name + ".png");
    c_charge->Print(path + batch + "charge_" + sensor_name + ".C");

};

void Plot::save_fig_eff_vs_charge(TGraph * gr, int channel, 
            TString path, TString sensor_name, 
            TString irradiation, 
            TString batch){
    TLatex *t = new TLatex();
    t->SetNDC();
    t->SetTextFont(62);
    t->SetTextColor(12);
    t->SetTextSize(0.04);
    t->SetTextAlign(12);
    TCanvas *c_eff_vs_charge =
        new TCanvas(Form("c_eff_vs_charge%d", channel), Form("c_eff_vs_charge%d", channel), 600, 600);
    c_eff_vs_charge->cd();
    gr->SetTitle("");
    gr->GetXaxis()->SetTitle("Charge [fC]");
    gr->GetYaxis()->SetTitle("Efficiency [%]");
    gr->GetYaxis()->SetTitleOffset(1.2);
    gr->SetLineColor(2);
    gr->SetLineWidth(2);
    gr->SetMarkerColor(2);
    gr->SetMarkerSize(1);
    gr->SetMarkerStyle(20);
    gr->SetMinimum(0);
    gr->SetMaximum(105);
    gr->Draw("ALP");
    t->DrawLatex(0.25, 0.65, sensor_name);
    t->DrawLatex(0.25, 0.6, "#phi = " + irradiation);
    c_eff_vs_charge->Print(path + batch + "eff_vs_charge_" + sensor_name + ".png");
    c_eff_vs_charge->Print(path + batch + "eff_vs_charge_" + sensor_name + ".C");

};

void Plot::save_fig_eff_projection(TH2F *hist_XY, int channel, 
            TString path, TString sensor_name, TString irradiation, TString batch, float efficiency){
    auto legend = new TLegend(0.2, 0.25, 0.4, 0.35);
    legend->SetBorderSize(0);
    TLatex *t = new TLatex();
    t->SetNDC();
    // t->SetTextFont(62);
    // t->SetTextColor(12);
    // t->SetTextSize(0.04);
    // t->SetTextAlign(12);

    Int_t firstbinx = hist_XY->GetXaxis()->FindBin(-0.25);
    Int_t lastbinx = hist_XY->GetXaxis()->FindBin(0.25);
    Int_t firstbiny = hist_XY->GetYaxis()->FindBin(-0.25);
    Int_t lastbiny = hist_XY->GetYaxis()->FindBin(0.25);
    TH1D *hist_proj_x = hist_XY->ProjectionX("_xp",firstbinx, lastbinx);
    TH1D *hist_proj_y = hist_XY->ProjectionY("_yp",firstbiny, lastbiny);
    TCanvas *c_eff_proj_x =
    new TCanvas(Form("c_eff_proj_x_%d", channel), Form("c_eff_vs_charge%d", channel), 600, 600);
    c_eff_proj_x->cd();
    hist_proj_x->SetTitle("");
    hist_proj_x->GetXaxis()->SetTitle("X");
    hist_proj_x->GetYaxis()->SetTitle("Efficiency [%]");
    hist_proj_x->GetYaxis()->SetTitleOffset(1.3);
    hist_proj_x->SetLineColor(2);
    hist_proj_x->SetLineWidth(2);
    hist_proj_x->SetMarkerColor(2);
    hist_proj_x->SetMarkerSize(1);
    hist_proj_x->SetMarkerStyle(20);
    hist_proj_x->Scale(1./21);
    hist_proj_x->Rebin(1);
    double eff = static_cast<double>(efficiency)*0.9978;
    Double_t binCenter_first = hist_proj_x->GetBinCenter(hist_proj_x->FindLastBinAbove(eff));
    Double_t binCenter_last = hist_proj_x->GetBinCenter(hist_proj_x->FindFirstBinAbove(eff));
    Double_t binCenter95_first = hist_proj_x->GetBinCenter(hist_proj_x->FindLastBinAbove(95.));
    Double_t binCenter95_last = hist_proj_x->GetBinCenter(hist_proj_x->FindFirstBinAbove(95.));

    std::cout<< "First x: " << binCenter_first << " last x: " << binCenter_last << " Eff: " << eff <<std::endl;
    std::cout<< "First x higher 95%: " << binCenter95_first << " last x: " << binCenter95_last << " Eff: " << eff <<std::endl;

    // if(channel == 0){   
    //     hist_proj_x->SetMarkerColor(kBlue+2);
    //     hist_proj_x->SetMarkerSize(0.5);
    //     hist_proj_x->SetMarkerStyle(20);   
    // }
    // if(channel == 1){   
    //     hist_proj_x->SetMarkerColor(kGreen+1);
    //     hist_proj_x->SetMarkerSize(0.5);
    //     hist_proj_x->SetMarkerStyle(33);   
    // }
    hist_proj_x->GetYaxis()->SetRangeUser(70,110);
    legend->AddEntry(hist_proj_x, sensor_name, "p");
    hist_proj_x->Draw("HIST p");
    legend->Draw();
    t->SetTextFont(72);
    t->SetTextColor(1);
    t->SetTextSize(0.05);
    t->SetTextAlign(12);
    t->DrawLatex(0.133,0.93,"ATLAS HGTD");
    t->SetTextFont(42);
    t->SetTextSize(0.05);
    t->DrawLatex(0.43,0.925,"Preliminary Test Beam");
    t->SetTextFont(62);
    t->SetTextColor(12);
    t->SetTextSize(0.04);
    // t->DrawLatex(0.35, 0.45, sensor_name);
    t->DrawLatex(0.2, 0.2, "#phi = " + irradiation);
    c_eff_proj_x->Print(path + batch + "eff_proj_x_" + sensor_name + ".png");
    c_eff_proj_x->Print(path + batch + "eff_proj_x_" + sensor_name + ".C");
    TCanvas *c_eff_proj_y =
        new TCanvas(Form("c_eff_proj_y_%d", channel), Form("c_eff_proj_y_%d", channel), 600, 600);
    c_eff_proj_y->cd();
    hist_proj_y->SetTitle("");
    hist_proj_y->GetXaxis()->SetTitle("Y");
    hist_proj_y->GetYaxis()->SetTitle("Efficiency [%]");
    hist_proj_y->GetYaxis()->SetTitleOffset(1.2);
    hist_proj_y->SetLineColor(2);
    hist_proj_y->SetLineWidth(2);
    hist_proj_y->SetMarkerColor(2);
    hist_proj_y->SetMarkerSize(1);
    hist_proj_y->SetMarkerStyle(20);
    hist_proj_y->Scale(1./21);
    binCenter_first = hist_proj_y->GetBinCenter(hist_proj_y->FindLastBinAbove(eff));
    binCenter_last = hist_proj_y->GetBinCenter(hist_proj_y->FindFirstBinAbove(eff));
    binCenter95_first = hist_proj_y->GetBinCenter(hist_proj_y->FindLastBinAbove(95.));
    binCenter95_last = hist_proj_y->GetBinCenter(hist_proj_y->FindFirstBinAbove(95.));
    std::cout<< "First y: " << binCenter_first << " last y: " << binCenter_last << " Eff: " << eff <<std::endl;
    std::cout<< "First y higher 95%: " << binCenter95_first << " last y: " << binCenter95_last << " Eff: " << eff <<std::endl;

    hist_proj_y->GetYaxis()->SetRangeUser(60,110);
    hist_proj_y->Draw("HIST, L");
    t->DrawLatex(0.35, 0.45, sensor_name);
    t->DrawLatex(0.35, 0.4, "#phi = " + irradiation);
    c_eff_proj_y->Print(path + batch + "eff_proj_y_" + sensor_name + ".png");
    c_eff_proj_y->Print(path + batch + "eff_proj_y_" + sensor_name + ".C");

};


