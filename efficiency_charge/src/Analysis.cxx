#include "Analysis.h"
#include <iostream>

void Analysis::Run(TString filename, std::array<int, N_OF_DEVICES> &channels,
                   std::array<TH2F *, N_OF_DEVICES> &hist_XY,
                   std::array<TGraph *, N_OF_DEVICES> &gr_eff_charge,
                   std::array<float, N_OF_DEVICES> &eff_charge, 
                   std::array<float, N_OF_DEVICES> &MPV,
                   std::array<RooPlot *, N_OF_DEVICES> &xframe) {
  ROOT::EnableImplicitMT();
  auto CutTimeMax = CutTimeMin;
  calculate_time_max(CutTimeMax);
  RDataFrame dataframe("tree", filename);

  // Fill the range of channels
  for (size_t i = 0; i < N_OF_DEVICES; i++) {
    channels.at(i) = i;
  }
  // Here I fill the histos with data after cut implementation
  for (int channel : channels) {

    //--------------  1. Find the center of the sensor -----------------//

    auto center_of_sensor = finding_center(dataframe, channel, CutTimeMax); //find the center of the sensor
    bool change_centre = false; // Put false if the center was found correct; put True if it needs to be adjusted
    if (change_centre)
    {
      if (channel == 0) { // Adjust center for the first sensor 
        center_of_sensor.at(0) = center_of_sensor.at(0) + (0./0.0185); //+ to move to the left; - to move to the right
        center_of_sensor.at(1) = center_of_sensor.at(1) - (0.04/0.0185); //+ to move down; - to move up
      } 
      if (channel == 1) { // Adjust center for the second sensor; if more sensors need to be changed, add another if
        center_of_sensor.at(0) = center_of_sensor.at(0) - (0.08/0.0185); //+ to move to the left; - to move to the right
        center_of_sensor.at(1) = center_of_sensor.at(1) + (0.0/0.0185); //+ to move down; - to move up
      }
    }
  
    //--------------  2. Apply the cuts on the events: 1.pulseHeight of SiPM  2. Reconstructed timeCFD difference between SiPM and a sensor -----------------//

    auto data_cut = apply_cuts(dataframe, channel, CutTimeMax);

    //--------------  3. Calculate the efficiency  in the center of the sensor (0.5mmx0.5mm) for each cut on charge -----------------//
    //--------------(see std::array<float, 6> cut_charge{{0.5, 1, 2, 3, 4, 5}} in Analysis.h ) -----------------//

    auto efficiency_vs_charge =
        efficiency_calculation(data_cut, center_of_sensor, channel);
    gr_eff_charge.at(channel) = effeciency_vs_charge_gr(efficiency_vs_charge);

    //--------------  4. Get the efficiency for cut on charge at 2fC -----------------//

    eff_charge.at(channel) = efficiency_vs_charge.at(2);
    
    //--------------  5. Create a histogram with the efficiency map with the cut on charge at 2fC -----------------//

    hist_XY.at(channel) = efficiency_map(data_cut, center_of_sensor, channel,
                                        nbins, bins);

    //--------------  6. Calculate the Most Probable Value of collected charge -----------------//

    MPV.at(channel) =
        charge_calculation(data_cut, channel, center_of_sensor, xframe);
  }
}

std::array<double, 2>
Analysis::finding_center(RNode dataframe, int channel,
                         std::array<float, N_OF_DEVICES> CutTimeMax) {
  auto data_cut_centering =
      dataframe
          .Filter(
              Form("pulseHeight[2]>%f && (timeCFD20[2]-timeCFD50[%d])>%f && "
                   "(timeCFD20[2]-timeCFD50[%d])<%f && charge[%d]>5",
                   pulseheightSiPM_cut, channel, CutTimeMin[channel], channel,
                   CutTimeMax[channel], channel))
          .Define("Y", Form("Ytr[%d]", channel))
          .Define("X", Form("Xtr[%d]", channel));
  auto mean_x = data_cut_centering.Mean("X");
  auto mean_y = data_cut_centering.Mean("Y");
  std::array<double, 2> mean_xy = {*mean_x, *mean_y};

  return mean_xy;
}

void Analysis::calculate_time_max(std::array<float, N_OF_DEVICES> &CutTimeMax) {
  for (auto &v : CutTimeMax) {
    v = v + 2000;
  }
}

RNode Analysis::apply_cuts(RNode df, int channel,
                           std::array<float, N_OF_DEVICES> CutTimeMax) {
  auto data_cut =
      df.Filter(Form("pulseHeight[2]>%f && (timeCFD20[2]-timeCFD50[%d])>%f "
                     "&& (timeCFD20[2]-timeCFD50[%d])<%f",
                     pulseheightSiPM_cut, channel, CutTimeMin[channel], channel,
                     CutTimeMax[channel]))
          .Define("Y", Form("Ytr[%d]", channel))
          .Define("X", Form("Xtr[%d]", channel));
  return data_cut;
}

std::array<float, 6>
Analysis::efficiency_calculation(RNode df,
                                 std::array<double, N_OF_DEVICES> centre,
                                 int channel) {
  auto Denumenator =
      df.Filter(Form("fabs(Ytr[%d]-%f)<13.5 && fabs(Xtr[%d]-%f)<13.5", channel,
                     centre[1], channel, centre[0]))
          .Count();
  auto deno = *Denumenator;
  std::array<float, 6> efficiency;

  for (int i = 0; i < cut_charge.size(); i++) {
    auto Numenator =
        df.Filter(Form("charge[%d]>%f && "
                       "fabs(Ytr[%d]-%f)<13.5 && fabs(Xtr[%d]-%f)<13.5",
                       channel, cut_charge[i], channel, centre[1], channel,
                       centre[0]))
            .Count();
    auto num = *Numenator;
    
    efficiency.at(i) = 100. * (float)num / (float)deno;
  }

  return efficiency;
};

TGraph *Analysis::effeciency_vs_charge_gr(const std::array<float, 6> efficiency_vs_charge){
  int n = 6;
  TGraph *grEffCh = new TGraph(n);
  for (size_t i = 0; i < n; i++)
  {
    grEffCh->SetPointX(i,cut_charge.at(i) );
    grEffCh->SetPointY(i,efficiency_vs_charge.at(i) );

  }
  return grEffCh;
}

TH2F *Analysis::efficiency_map(RNode df,
                               std::array<double, N_OF_DEVICES> centre,
                               int channel, int nbins,
                               std::array<int, N_OF_BINS> bins) {
  auto denu =
      df.Filter(Form("fabs(Ytr[%d]-%f)<54 && fabs(Xtr[%d]-%f)<54", channel,
                     centre[1], channel, centre[0]))
          .Define("DenoY",
                  Form("Ytr[%d] * 0.0185 - %f * 0.0185", channel, centre[1]))
          .Define("DenoX",
                  Form("Xtr[%d] * 0.0185 - %f * 0.0185", channel, centre[0]));
  auto nume =
      df.Filter(
            Form("charge[%d]>2 && fabs(Ytr[%d]-%f)<54 && fabs(Xtr[%d]-%f)<54",
                 channel, channel, centre[1], channel, centre[0]))
          .Define("NumY",
                  Form("Ytr[%d] * 0.0185 - %f * 0.0185", channel, centre[1]))
          .Define("NumX",
                  Form("Xtr[%d] * 0.0185 - %f * 0.0185", channel, centre[0]));

  auto hist_XY_denu =
      denu.Histo2D({"deno", "denu", nbins, (-1) * 54 * 0.0185, 54 * 0.0185, nbins,
                    54 * (-1) * 0.0185, +54 * 0.0185},
                   "DenoX", "DenoY");
  // hist_XY_denu->SetMinimum(5);
  auto hist_XY_num =
      nume.Histo2D({"num", "num", nbins, 54 * (-1) * 0.0185, 54 * 0.0185, nbins,
                    54 * (-1) * 0.0185, 54 * 0.0185},
                   "NumX", "NumY");
  // hist_XY_num->SetMinimum(5);
  TH2F *h1 = (TH2F *)hist_XY_num->Clone();
  TH2F *h2 = (TH2F *)hist_XY_denu->Clone();

  for (int i = 0; i < nbins; i++) {
    bins.at(i) = i + 1;
  }
  for (auto bin_x : bins) {
    for (auto bin_y : bins) {
      if (h1->GetBinContent(bin_x, bin_y) < cut_on_occupancy) {
        h1->SetBinContent(bin_x, bin_y, 0);
      }
    }
  }

  h1->Divide(h2);
  h1->Scale(100);

  return h1;
}

float Analysis::charge_calculation(
    RNode df, int channel, std::array<double, N_OF_DEVICES> centre,
    std::array<RooPlot *, N_OF_DEVICES> &xframe) {

  RooMsgService::instance().setGlobalKillBelow( RooFit::FATAL); // silense Roofit output
  RooMsgService::instance().setSilentMode(true); // silense RooMinuit output
  auto charge_cut =
      df.Filter(Form("fabs(Ytr[%d]-%f)<13.5 && fabs(Xtr[%d]-%f)<13.5", channel,
                     centre[1], channel, centre[0]))
          .Define("chargeMPV", Form("charge[%d]", channel));
  auto MaxCh = MinCh.at(channel) + 50;
  auto HChargeDist = charge_cut.Histo1D(
      {"charge", "charge", 50, MinCh.at(channel), MaxCh},
      "chargeMPV");
  RooRealVar x("x", "Charge [fC]", MinCh[channel], MaxCh);
  xframe.at(channel) = x.frame();
  TH1F *h1 = (TH1F *)HChargeDist->Clone();
  RooDataHist *dh = new RooDataHist("dh", "dh", x, RooFit::Import(*h1));
  RooRealVar ml("MPV", "mean landau", mean_charge_guess[channel],
                mean_charge_guess[channel] - 5, mean_charge_guess[channel] + 5);
  RooRealVar sl("sl", "sigma landau", 1, 0.1, 10);
  RooLandau landau("lx", "lx", x, ml, sl);
  RooRealVar mg("mg", "mg", 0);
  RooRealVar sg("sg", "sg", 2, 0.1, 10);
  RooGaussian gauss("gauss", "gauss", x, mg, sg);
  RooFFTConvPdf lxg("lxg", "landau (X) gauss", x, landau, gauss);
  RooFitResult *res = lxg.fitTo(*dh);
  // RooBinSamplingPdf binSampler("lxg_bin", "lxg_bin", x, lxg, 1.E-5);
  // binSampler.fitTo(*dh);
  // binSampler.plotOn(xframe.at(channel));
  dh->plotOn(xframe.at(channel));
  lxg.plotOn(xframe.at(channel));
  
  // lxg.paramOn(xframe.at(channel), RooFit::Parameters(ml), RooFit::Layout(0.5,
  // 0.85, 0.7)); lxg.paramOn(xframe.at(channel), RooFit::Parameters(sl),
  // RooFit::Layout(0.5, 0.85, 0.6)); lxg.paramOn(xframe.at(channel),
  // RooFit::Parameters(sg), RooFit::Layout(0.5, 0.85, 0.5));
  return ml.getValV();
} 
void Analysis::write_to_file(TString path, TString sensor_name, TString BV, float efficiency, float MPV){
  std::ofstream myfile;
  myfile.open(path + sensor_name + ".txt", std::ios::app);
  myfile << BV << " " << efficiency << " " << MPV <<std::endl;
  myfile.close();
}

void Analysis::write_parameters(TString path, TString sensor_name, TString batch, float CutTimeMin, float MinCh, float mean_charge_guess){
  std::ofstream myfile;
  myfile.open(path + batch + sensor_name + "_parameters.txt", std::ios::trunc);
  myfile << "CutTimeMin" << " " << "pulseheightSiPM_cut" << " " << "MinCh" << " "<< "mean_charge_guess" <<std::endl;
  myfile << CutTimeMin << " " << pulseheightSiPM_cut << " " << MinCh << " "<< mean_charge_guess <<std::endl;
  myfile.close();
}
//#endif