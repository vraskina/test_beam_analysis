file(GLOB SOURCES "*.cxx")

add_library("${PROJECT_NAME}Lib" ${SOURCES})

target_include_directories ("${PROJECT_NAME}Lib" PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
