#pragma once
#ifndef _PLOT_H
#define _PLOT_H
#include "Analysis.h"
#include <TLatex.h>
#include "TSystem.h"
#include <TCanvas.h>
#include "TLegend.h"

class Plot : public Analysis {

public:

void save_fig_efficiency(TH2F *hist_XY, int channel, 
            TString path, TString sensor_name, 
            TString irradiation, TString batch);
void save_fig_charge(RooPlot *xframe, int channel, 
            TString path, TString sensor_name, 
            TString irradiation, TString batch, float MPV);
void save_fig_eff_vs_charge(TGraph *gr, int channel, 
            TString path, TString sensor_name, 
            TString irradiation, TString batch);
void save_fig_eff_projection(TH2F *hist_XY, int channel, 
            TString path, TString sensor_name, 
            TString irradiation, 
            TString batch, float efficiency);

};

#endif