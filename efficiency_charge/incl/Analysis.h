#pragma once
#ifndef _ANALYSIS_H
#define _ANALYSIS_H
#include "ROOT/RDF/RInterface.hxx"
#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include "Riostream.h"
#include "RooBinIntegrator.h"
#include "RooBinSamplingPdf.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooFFTConvPdf.h"
#include "RooFit.h"
#include "RooGaussian.h"
#include "RooIntegrator1D.h"
#include "RooLandau.h"
#include "RooMinuit.h"
#include "RooNumIntConfig.h"
#include "RooNumIntFactory.h"
#include "RooNumber.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "TMinuit.h"
#include <TCanvas.h>
#include <TGraph.h>
#include <fstream>
#define N_OF_DEVICES 2 // put the amount of devices here
#define N_OF_BINS 81  
using namespace ROOT;
using RNode = ROOT::RDF::RNode;

class Analysis : public TObject {
// class Analysis {
public:
  // Analysis();
  // ~Analysis();

  // To be changed in every batch!

  std::array<float, N_OF_DEVICES> CutTimeMin{{0, 0}};
  Float_t pulseheightSiPM_cut = 5;
  std::array<float, 6> cut_charge{{0.5, 1, 2, 3, 4, 5}};
  int cut_on_occupancy = 4;
  std::array<float, N_OF_DEVICES> MinCh{{5, 0}};
  // std::array<float, N_OF_DEVICES> MaxCh{{55, 50}};
  std::array<float, N_OF_DEVICES> mean_charge_guess{{14, 7}};
  // Don't change below
  std::array<int, N_OF_DEVICES> channels;
  // std::array<ROOT::RDF::RResultPtr<TH2D>, N_OF_DEVICES> hist_XY;
  const int nbins = N_OF_BINS;
  std::array<int, N_OF_BINS> bins;
  void Run(TString filename, std::array<int, N_OF_DEVICES> &channels,
                   std::array<TH2F *, N_OF_DEVICES> &hist_XY,
                   std::array<TGraph *, N_OF_DEVICES> &gr_eff_charge,
                   std::array<float, N_OF_DEVICES> &eff_charge, 
                   std::array<float, N_OF_DEVICES> &MPV,
                   std::array<RooPlot *, N_OF_DEVICES> &xframe);
  std::array<double, N_OF_DEVICES>
  finding_center(RNode dataframe, int channel,
                 std::array<float, N_OF_DEVICES> CutTimeMax);
  void calculate_time_max(std::array<float, N_OF_DEVICES> &CutTimeMax);
  RNode apply_cuts(RNode df, int channel, 
                   std::array<float, N_OF_DEVICES> CutTimeMax);
  std::array<float, 6>
  efficiency_calculation(RNode df, std::array<double, N_OF_DEVICES> centre,
                         int channel);
  TGraph *effeciency_vs_charge_gr(std::array<float, 6> efficiency_vs_charge);
  TH2F *efficiency_map(RNode df, std::array<double, N_OF_DEVICES> centre,
                       int channel, int nbins, std::array<int, N_OF_BINS> bins);
  float charge_calculation(RNode df, int channel,
                           std::array<double, N_OF_DEVICES> centre,
                           std::array<RooPlot *, N_OF_DEVICES> &xframe);
  void write_to_file(TString path, TString sensor_name, TString BV, 
                                     float efficiency, float MPV);

  void write_parameters(TString path, TString sensor_name, TString batch, float CutTimeMin, float MinCh, float mean_charge_guess);

private:
  ClassDef(Analysis, 1);
};

#endif