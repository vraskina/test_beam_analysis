# Test_Beam_Analysis

This is the Test Beam analysis package for the HGTD

<!-- **Root version supporting RooFitMore is needed:**
source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.22.06/x86_64-centos7-gcc48-opt/bin/thisroot.sh -->


<!-- Runanalyse.cpp calculates the efficiency in the center of the tested sensors together with plotting Occupancy, Efficiency, Efficiency vs Charge, Central efficiency and the charge distribution fitted with Landau(x)Gaussian. -->
**Building the package for the efficiency and charge analysis:**

mkdir `test_beam`

cd `test_beam`

git clone https://:@gitlab.cern.ch:8443/vraskina/test_beam_analysis.git

cd `efficiency_charge`

source `setup.sh`

mkdir build

cd build

source ../CMakeLists.txt

make -j4

**Execute the analysis:**

cd build/exe

./run_analysis

**Variables to be changed before analysing:**

The analysis is orchestrated in `src/Analysis.cxx` and `incl/Analysis.h`, the plots are done in `src/Plot.cxx` and `incl/Plot.h`, the package is run in `exe/run_analysis.cxx` and `exe/run_analysis.h`.

In the `incl/Analysis.h`, change  `N_OF_DEVICES` to amount of the devices
In the `exe/run_analysis.h`, change: `sensor_name` to the names of devices, `irradiation` for irradiation level, `BV` for bias voltages, `path` to the path where the batches folders are, `batch` where the root file is and where to save the plots, `filename` is the root file with data.

In the `exe/run_analysis.cxx`, change the `CutTimeMin` for the minimum on reconstructed timeCFD difference between SiPM and a sensor (usually 0), `pulseheightSiPM_cut` for the cut on pulseheightSiPM, `MinCh` for the minimum charge in the charge distribution histogram, `mean_charge_guess` for the guess of the mean charge (to be checked in `tree->Draw(“charge[sensor]”,”pulseHeight[SiPM]>5 && (timeCFD20[SiPM]-timeCFD50[sensor])>0 && (timeCFD20[SiPM]-timeCFD50[sensor])<2000  && fabs(Ytr[sensor]-center)<100 && fabs(Xtr[sensor]-center)<100","colz")`)

**Running the analysis for time resolution:**

In the output file : BV, resolution, error of resolution, start of the gauss fit, end of the gauss fit

`python3 reso_iminuit2.py -i *.root -o output_file.txt
`
<!-- - Xzerot[i] - X coordinate of the sensor (pad) or DUT, to be checked:
tree->Draw("Xtr[i]","pulseHeight[SiPM]>20 && (timeAtMax[SiPM]-timeAtMax[i])>3000 && (timeAtMax[SiPM]-timeAtMax[i])<5000 ")

- Yzerot[i] - Y coordinate of the sensor (pad) or DUT, to be checked:
tree->Draw("Ytr[i]","pulseHeight[SiPM]>20 && (timeAtMax[SiPM]-timeAtMax[i])>3000 && (timeAtMax[SiPM]-timeAtMax[i])<5000 ")

- CutTimeMin[i] - minimum difference between time when pulse reaches the peak in the DUT and SIPM, to be checked:
tree->Draw("(timeAtMax[SiPM]-timeAtMax[i])","pulseHeight[SiPM]>20")

- MinCh[i] - minimal collected charge, to be checked:
tree->Draw("charge[i]","pulseHeight[SiPM]>20 && (timeAtMax[SiPM]-timeAtMax[i])>3000 && (timeAtMax[SiPM]-timeAtMax[i])<5000 && fabs(Ytr[i]+6.55)<1 && fabs(Xtr[i]+7.75)<1")
- mean_landau[i] - MPV, the peak of charge distribution. Just from the eye, the real value will be calculated by RooFit. -->
