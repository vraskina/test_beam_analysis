#define Runanalyse_cxx
#include "Runanalyse.h"
#include "../../atlasstyle-00-03-05/AtlasStyle.C"
#include "../../atlasstyle-00-03-05/AtlasUtils.h"
#include "Riostream.h"
#include "RooBinIntegrator.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooFFTConvPdf.h"
#include "RooFit.h"
#include "RooGaussian.h"
#include "RooIntegrator1D.h"
#include "RooLandau.h"
#include "RooMsgService.h"
#include "RooNumIntConfig.h"
#include "RooNumber.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TVirtualFFT.h"
#include <TCanvas.h>
#include <TColor.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TH2.h>
#include <TLatex.h>
#include <TLine.h>
#include <TMath.h>
#include <TMinuit.h>
#include <TStyle.h>
using namespace RooFit;

void Runanalyse::Loop() {
  //.L Runanalyse.C+
  // Runanalyse * m = new Runanalyse(0 , 104)
  // m->Loop()
  //   In a ROOT session, you can do:
  //      root> .L Runanalyse.C
  //      root> Runanalyse(304) t
  //      root> t.GetEntry(12); // Fill t data members with entry number 12
  //      root> t.Show();       // Show values of entry 12
  //      root> t.Show(16);     // Read and show values of entry 16
  //      root> t.Loop();       // Loop on all entries
  //
  // se lance avec .L Runanalyse.C+
  //  Runanalyse * m = new Runanalyse(0 , 304) puis m->Loop()
  //   ou  Runanalyse m(0, 304) puis m.Loop()

  // Valeurs à changer
  const Double_t ratio = 2.0; // taille d'un bin en pixel
  TString Batchnumber = TString("batch") + TString::Format("%d", batchNum);
  const Int_t nbchannels = 3;
  Int_t channel[nbchannels] = {0, 1, 2};

  Double_t Xzerot[4];     // Centre du DUT en bin
  Double_t Yzerot[4];     // Centre du DUT en bin
  Double_t CutTimeMax[4]; // bornes pour le Delta t avec le SiPM
  Double_t CutTimeMin[4]; //
  Double_t CutAmplMin[4];
  Double_t CutAmplMax[4];
  const Int_t NbinCh = 50.;
  Double_t MinCh[4];
  Double_t MaxCh[4];
  Double_t mean_landau[3];

  for (Int_t i = 0; i < 4; i++) {
    Xzerot[i] = 0.;
    Yzerot[i] = 0.;
    CutTimeMin[i] = -1.;
    CutTimeMax[i] = -1.;
    CutAmplMin[i] = 0.;
    CutAmplMax[i] = 0.;
  }
  Xzerot[0] = -8.7;
  Yzerot[0] = -6.55;
  CutAmplMin[0] = 30.;
  CutTimeMin[0] = 3550;
  MinCh[0] = 4;
  MaxCh[0] = MinCh[0] + 50;
  mean_landau[0] = 14;

  Xzerot[1] = -8.25;
  Yzerot[1] = -5.65;
  CutAmplMin[1] = 10.;
  CutTimeMin[1] = 1200.;
  MinCh[1] = -42;
  MaxCh[1] = MinCh[1] + 50;
  mean_landau[1] = -42;

  Xzerot[2] = -8.19;
  Yzerot[2] = -5.75;
  CutAmplMin[2] = 30.;
  CutTimeMin[2] = 3250.;
  MinCh[2] = -4;
  MaxCh[2] = MinCh[2] + 50;
  mean_landau[2] = 4;

  for (Int_t i = 0; i < nbchannels; i++) {
    CutTimeMax[channel[i]] = CutTimeMin[channel[i]] + 2000.;
  }

  // SiPM
  CutAmplMin[3] = 20.;
  //
  CutAmplMax[3] = 770.;

  TString tnamesensor[4];
  tnamesensor[0] = TString("W0LGA35");
  tnamesensor[1] = TString("deb1");
  tnamesensor[2] = TString("deb2");
  tnamesensor[3] = TString("SIPM1");

  tnamesensor[1] = TString("HPK W28");
  tnamesensor[2] = TString("FBK W14");

  TString tbuildsensor[4];
  tbuildsensor[0] = TString("");
  tbuildsensor[1] = TString("");
  tbuildsensor[2] = TString("UFSD3.2");
  tbuildsensor[3] = TString("SIPM1");

  tbuildsensor[2] = TString("");

  TString tirrsensor[4];
  tirrsensor[0] = TString("unIrr");
  tirrsensor[1] = TString("Irr 1.5e15n");
  tirrsensor[2] = TString("Irr 1.5e15n");
  tirrsensor[3] = TString("");

  tirrsensor[2] = TString("Irr 1.5e15n");

  // fin des choses à changer
  cout << Batchnumber << " DUT etudiés : " << endl;
  for (Int_t i = 0; i < nbchannels; i++) {
    cout << "canal " << channel[i] << " DUT name " << tnamesensor[channel[i]]
         << " Irradiation " << tirrsensor[channel[i]] << endl;
  }

  Int_t nbBin = 108;
  Double_t conversionmicron = 0.0185;
  Double_t CutCharge[6] = {0.5, 1., 2., 3., 4., 5}; // cut sur la charge en fC
  Double_t cutoccup = 1.;                           // try 2 as a cut
  if (ratio == 2)
    cutoccup = 4; // 3-4
  if (ratio == 3)
    cutoccup = 5; // 5

  Double_t Xmin[4]; // bornes de l'histo : calculées pour faire -1 -> +1 mm
  Double_t Xmax[4];
  Double_t Ymin[4];
  Double_t Ymax[4];

  for (Int_t i = 0; i < 4; i++) {
    Ymax[i] = +0.999;
    Xmax[i] = +0.999;
    Ymin[i] = -0.999;
    Xmin[i] = -0.999;
  }

  // Pour les Cut en X
  Double_t CutMinX[4];
  Double_t CutMaxX[4];
  Double_t CutMinY[4];
  Double_t CutMaxY[4];

  for (Int_t i = 0; i < 4; i++) {
    CutMinX[i] = Xzerot[i] - 0.999;
    CutMinY[i] = Yzerot[i] - 0.999;
    CutMaxX[i] = Xzerot[i] + 0.999;
    CutMaxY[i] = Yzerot[i] + 0.999;
  }

  const Int_t Nbinx = nbBin / ratio; // nombre de bin
  const Int_t Nbiny = nbBin / ratio;

  TH2F *HEffic[4];
  TH2F *HEfficCentral[4];
  TH2F *HOccup[4];
  TH1F *HChargeDist[4];
  // names of the histograms
  TString tnameeffic[4];
  TString tnameefficCentral[4];
  TString tnameoccup[4];
  TString tnamecharge[4];

  // names of the png
  TString tnameefficpng[4];
  TString tnameefficCentralpng[4];
  TString tnameefficvschargepng[4];
  TString tnameoccuppng[4];
  TString tnamechargepng[4];

  for (Int_t i = 0; i < 4; i++) {
    // names of the histo
    tnameeffic[i] = tnamesensor[i];
    tnameefficCentral[i] = tnamesensor[i];
    tnameoccuppng[i] = tnamesensor[i];
    tnamechargepng[i] = tnamesensor[i] + "_Charge";
    tnameeffic[i] += "_Efficiency";
    tnameefficCentral[i] += "_CentralEfficiency";
    tnameoccuppng[i] += "_Occupancy";
    HEffic[i] = new TH2F(tnameeffic[i], tnameeffic[i], Nbinx, Xmin[i], Xmax[i],
                         Nbiny, Ymin[i], Ymax[i]);
    HEfficCentral[i] =
        new TH2F(tnameefficCentral[i], tnameefficCentral[i], Nbinx, Xmin[i],
                 Xmax[i], Nbiny, Ymin[i], Ymax[i]);
    tnameoccup[i] = tnamesensor[i];
    tnameoccup[i] += "Occupancy";
    tnamecharge[i] = tnamesensor[i] + "_Charge";
    HOccup[i] = new TH2F(tnameoccup[i], tnameoccup[i], Nbinx, Xmin[i], Xmax[i],
                         Nbiny, Ymin[i], Ymax[i]);
    HChargeDist[i] =
        new TH1F(tnamecharge[i], tnamecharge[i], NbinCh, MinCh[i], MaxCh[i]);
    // names of the png
    tnameefficpng[i] = tnameeffic[i] + "_" + Batchnumber + "_" +
                       TString::Format("%.0f", ratio) + "pixelEutel.png";
    //
    tnameefficCentralpng[i] = tnameefficCentral[i] + "_" + Batchnumber + "_" +
                              TString::Format("%.0f", ratio) + "pixelEutel.png";
    //
    tnameefficvschargepng[i] = "Effic_vs_charge_";
    tnameefficvschargepng[i] +=
        tnamesensor[i] + "-" + Batchnumber + "Eutel.png";
    //
    tnameoccuppng[i] = tnameoccuppng[i] + "_" + Batchnumber + "_" +
                       TString::Format("%.0f", ratio) + "pixelEutel.png";
    ;
  }

  Double_t Occup[Nbinx][Nbiny][4];
  Double_t Deno[Nbinx][Nbiny][4];

  // Effic centrale en fonction du cut sur la charge (4-> nb de DUT, 6 : nb de
  // cut sur charge)

  Double_t OccupCentre[4][6];
  Double_t DenoCentre[4];
  Double_t EfficCentre[4][6];
  Double_t ErrEffic[4][6];

  for (Int_t i5 = 0; i5 < 4; i5++) {
    DenoCentre[i5] = 0;
    for (Int_t ic = 0; ic < 6; ic++) {
      OccupCentre[i5][ic] = 0;
      EfficCentre[i5][ic] = 0;
      ErrEffic[i5][ic] = 0;
    }
    for (Int_t iy = 0; iy < Nbiny; iy++) {
      for (Int_t ix = 0; ix < Nbinx; ix++) {
        Occup[ix][iy][i5] = 0.;
        Deno[ix][iy][i5] = 0.;
      }
    }
  }

  Int_t BinX[4];
  Int_t BinY[4];

  if (fChain == 0)
    return;
  Long64_t nentries = fChain->GetEntriesFast();
  Long64_t nbytes = 0, nb = 0;
  cout << " nentries " << nentries << endl;
  for (Long64_t jentry = 0; jentry < nentries; jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0)
      break;
    nb = fChain->GetEntry(jentry);
    nbytes += nb;
    // if (Cut(ientry) < 0) continue;

    for (Int_t i5 = 0; i5 < 4; i5++) {
      BinX[i5] = 0;
      BinY[i5] = 0;
    }

    for (Int_t i5 = 0; i5 < 4; i5++) {

      // Definition du no de bin
      if ((i5 != 3)) {
        BinX[i5] = TMath::FloorNint((Xtr[i5] - CutMinX[i5]) * Nbinx / 1.998);
        BinY[i5] = TMath::FloorNint((Ytr[i5] - CutMinY[i5]) * Nbiny / 1.998);
        if (BinX[i5] >= 0 && BinX[i5] < Nbinx && BinY[i5] < Nbiny &&
            BinY[i5] >= 0) {
          if (CutTimeMax[i5] != -1.) {
            if (pulseHeight[3] > CutAmplMin[3] &&
                pulseHeight[3] < CutAmplMax[3] &&
                (timeAtMax[3] - timeAtMax[i5]) < CutTimeMax[i5] &&
                (timeAtMax[3] - timeAtMax[i5]) > CutTimeMin[i5]) {
              // on se place dans la region sans bruit
              if (BinX[i5] <= (Nbinx * 0.625) && BinX[i5] >= (Nbinx * 0.375) &&
                  BinY[i5] >= (Nbiny * 0.375) && BinY[i5] <= (Nbiny * 0.625)) {
                DenoCentre[i5] =
                    DenoCentre[i5] + 1; // Deno de la prtie centrale
                HChargeDist[i5]->Fill(charge[i5]);
              }
              Deno[(BinX[i5])][(BinY[i5])][i5] =
                  Deno[(BinX[i5])][(BinY[i5])][i5] +
                  1.; // Denominateur bin par bin

              if (fabs(charge[i5]) >= 2) {
                Occup[(BinX[i5])][(BinY[i5])][i5] =
                    Occup[(BinX[i5])][(BinY[i5])][i5] +
                    1.; // numerateur bin par bin
                // cout << "i5 " << i5 << " (BinX[i5]) " << (BinX[i5]) << "
                // Xtr[i5] "  << Xtr[i5] <<   " (BinY[i5]) " << (BinY[i5]) << "
                // Ytr[i5] " << Ytr[i5]  <<  " Occup[(BinX[i5])][(BinY[i5])][i5]
                // " << Occup[(BinX[i5])][(BinY[i5])][i5] << endl;
              }
              if (BinX[i5] <= (Nbinx * 0.625) && BinX[i5] >= (Nbinx * 0.375) &&
                  BinY[i5] >= (Nbiny * 0.375) &&
                  BinY[i5] <=
                      (Nbiny * 0.625)) { // numerateur ds la partie centrale
                for (Int_t ic = 0; ic < 6;
                     ic++) { // on teste plusieurs cuts sur la  charge
                  if (fabs(charge[i5]) >= CutCharge[ic])
                    OccupCentre[i5][ic] = OccupCentre[i5][ic] + 1.;
                }
              }
            }
          }
        }
      }
    }
  } // end of loop
  // ON compute les resultats
  for (Int_t i = 0; i < nbchannels; i++) {
    cout << tnamesensor[channel[i]] << " effic "
         << " charge ";
    for (Int_t ic = 0; ic < 6; ic++) {
      if (DenoCentre[channel[i]] > 0.) {
        EfficCentre[channel[i]][ic] =
            100. * OccupCentre[channel[i]][ic] / DenoCentre[channel[i]];
        cout << " " << CutCharge[ic] << " " << EfficCentre[channel[i]][ic];
        ErrEffic[channel[i]][ic] =
            100 * sqrt((0.01 * EfficCentre[channel[i]][ic]) *
                       (1 - (0.01 * EfficCentre[channel[i]][ic])) /
                       DenoCentre[channel[i]]);
      }
    }
    cout << " " << endl;
  }

  Double_t effic[4];
  Double_t efficbin[4];
  Double_t Deno_tot[4];
  Double_t Num_tot[4];

  for (Int_t i5 = 0; i5 < 4; i5++) {
    effic[i5] = 0.;
    efficbin[i5] = 0.;
    Deno_tot[i5] = 0;
    Num_tot[i5] = 0;
  }

  for (Int_t iy = 0; iy < Nbiny; iy++) {
    for (Int_t ix = 0; ix < Nbinx; ix++) {
      for (Int_t i5 = 0; i5 < 3; i5++) {
        if (Occup[ix][iy][i5] > 1)
          HOccup[i5]->SetBinContent(ix + 1, iy + 1, Occup[ix][iy][i5]);
        if (Deno[ix][iy][i5] > 0) {
          if (Occup[ix][iy][i5] > cutoccup) {
            efficbin[i5] = Occup[ix][iy][i5] / Deno[ix][iy][i5];
            HEffic[i5]->SetBinContent(ix + 1, iy + 1, 100 * efficbin[i5]);
            if (ix <= (Nbinx * 0.625) && ix >= (Nbinx * 0.375) &&
                iy >= (Nbiny * 0.375) && iy <= (Nbiny * 0.625)) {
              HEfficCentral[i5]->SetBinContent(ix + 1, iy + 1,
                                               100 * efficbin[i5]);
            }
          }
          // on calcule l'effic au plateau)
          if (ix <= (Nbinx * 0.625) && ix >= (Nbinx * 0.375) &&
              iy >= (Nbiny * 0.375) && iy <= (Nbiny * 0.625)) {
            Deno_tot[i5] = Deno_tot[i5] + Deno[ix][iy][i5];
            Num_tot[i5] = Num_tot[i5] + Occup[ix][iy][i5];
          }
          // Debut des effic dans les TH2F
        }
      } // fin de deno>0
    }
  } // for (Int_t ix=0; ix<Nbinx; ix++) {

  Float_t ValeurEfficCentral[4];
  Float_t ErreurEfficCentral[4];
  Double_t Errcut[4];
  TString tvaleureffic[4];
  for (Int_t i5 = 0; i5 < 4; i5++) {
    ValeurEfficCentral[i5] = 0.;
    ErreurEfficCentral[i5] = 0.;
    Errcut[i5] = 0.;
  }
  for (Int_t i = 0; i < nbchannels; i++) {
    Int_t inb = channel[i];
    ValeurEfficCentral[inb] = Float_t(Num_tot[inb] * 100.) / Deno_tot[inb];
    ErreurEfficCentral[inb] =
        100 * sqrt(0.01 * ValeurEfficCentral[inb] *
                   (1 - 0.01 * ValeurEfficCentral[inb]) / Deno_tot[inb]);
    cout << tnamesensor[inb] << " effic " << Float_t(Num_tot[inb]) << " / "
         << Float_t(Deno_tot[inb]) << " = " << ValeurEfficCentral[inb]
         << " +/- " << ErreurEfficCentral[inb] << endl;

    tvaleureffic[inb] = "(";
    tvaleureffic[inb] +=
        TString::Format("%.1f", ValeurEfficCentral[inb]) + "+/-" +
        TString::Format("%.1f", ErreurEfficCentral[inb]) + ") %";
    cout << "erreur calculee direct " << EfficCentre[inb][2] << " ± "
         << ErrEffic[inb][2] << endl; // enlever
  }

  TGraphErrors *GEfficCharge[4];

  for (Int_t i5 = 0; i5 < 3; i5++) {
    GEfficCharge[i5] =
        new TGraphErrors(6, CutCharge, EfficCentre[i5], Errcut, ErrEffic[i5]);
    GEfficCharge[i5]->SetLineColor(2);
    GEfficCharge[i5]->SetLineWidth(2);
    GEfficCharge[i5]->SetTitle("Efficiency vs Cut on Charge");
    GEfficCharge[i5]->GetXaxis()->SetTitle("Cut on Charge (fC)");
    GEfficCharge[i5]->GetYaxis()->SetTitle("Efficiency (%)");
    GEfficCharge[i5]->SetMarkerColor(2);
    GEfficCharge[i5]->SetMarkerSize(1);
    GEfficCharge[i5]->SetMarkerStyle(20);
    GEfficCharge[i5]->SetMinimum(0.);
    if (EfficCentre[i5][0] > 95.)
      GEfficCharge[i5]->SetMaximum(105.);
    if (EfficCentre[i5][0] <= 95.)
      GEfficCharge[i5]->SetMaximum(100.);
  }

  RooPlot *xframe[3];
  RooDataHist *dh[3];
  for (Int_t i5 = 0; i5 < 3; i5++) {
    RooRealVar x("x", "Charge [fC]", MinCh[i5], MaxCh[i5]);
    xframe[i5] = x.frame();
    dh[i5] = new RooDataHist("dh" + tnamesensor[i5], "dh" + tnamesensor[i5], x,
                             Import(*HChargeDist[i5]));
    RooRealVar ml("MPV", "mean landau", mean_landau[i5], mean_landau[i5] - 5,
                  mean_landau[i5] + 5);
    RooRealVar sl("sl", "sigma landau", 1, 0.1, 10);
    RooLandau landau("lx", "lx", x, ml, sl);
    RooRealVar mg("mg", "mg", 0);
    RooRealVar sg("sg", "sg", 2, 0.1, 10);
    RooGaussian gauss("gauss", "gauss", x, mg, sg);
    RooFFTConvPdf lxg("lxg", "landau (X) gauss", x, landau, gauss);
    RooFitResult *res = lxg.fitTo(*dh[i5]);
    dh[i5]->plotOn(xframe[i5]);
    lxg.paramOn(xframe[i5], Parameters(ml), Layout(0.5, 0.85, 0.7));
    lxg.plotOn(xframe[i5]);
  }

  SetAtlasStyle();
  TLatex *t = new TLatex();
  t->SetNDC();
  t->SetTextFont(62);
  t->SetTextColor(12);
  t->SetTextSize(0.04);
  t->SetTextAlign(12);

  cout << "styles are set " << endl;

  Double_t w = 900;
  Double_t h = 900;

  TCanvas *c603[nbchannels];
  TString Canva603[nbchannels];
  TCanvas *c604[nbchannels];
  TString Canva604[nbchannels];
  TCanvas *c605[nbchannels];
  TString Canva605[nbchannels];
  TCanvas *c606[nbchannels];
  TString Canva606[nbchannels];
  TCanvas *c607[nbchannels];
  TString Canva607[nbchannels];

  for (Int_t i = 0; i < nbchannels; i++) {
    Canva603[i] = "c603_";
    Canva603[i] += tnamesensor[channel[i]];
    c603[i] = new TCanvas(Canva603[i], Canva603[i], w, h);
    Canva604[i] = "c604_";
    Canva604[i] += tnamesensor[channel[i]];
    c604[i] = new TCanvas(Canva604[i], Canva604[i], w, h);
    Canva605[i] = "c605_";
    Canva605[i] += tnamesensor[channel[i]];
    c605[i] = new TCanvas(Canva605[i], Canva605[i], w, h);
    Canva606[i] = "c606_";
    Canva606[i] += tnamesensor[channel[i]];
    c606[i] = new TCanvas(Canva606[i], Canva606[i], w, h);
    Canva607[i] = "c607_" + tnamesensor[channel[i]];
    c607[i] = new TCanvas(Canva607[i], Canva607[i], w, h);
  }

  //  c604_2->SetRightMargin(0.18);
  for (Int_t i = 0; i < 4; i++) {
    HEfficCentral[i]->GetXaxis()->SetTitle("X [mm]");
    HEfficCentral[i]->GetYaxis()->SetTitle("Y [mm]");
    HEfficCentral[i]->GetZaxis()->SetTitleOffset(0);
    HEfficCentral[i]->GetYaxis()->SetTitleOffset(1.9);
    HEffic[i]->GetXaxis()->SetTitle("X [mm]");
    HEffic[i]->GetYaxis()->SetTitle("Y [mm]");
    HEffic[i]->GetZaxis()->SetTitleOffset(0);
    HEffic[i]->GetYaxis()->SetTitleOffset(1.9);
    HOccup[i]->GetXaxis()->SetTitle("X [mm]");
    HOccup[i]->GetYaxis()->SetTitle("Y [mm]");
    HOccup[i]->GetZaxis()->SetTitleOffset(0);
    HOccup[i]->GetYaxis()->SetTitleOffset(1.9);
    HOccup[i]->SetMinimum(6.);
  }

  HEfficCentral[1]->SetMinimum(5.);
  HEfficCentral[1]->SetMaximum(100.);
  HEfficCentral[2]->SetMinimum(5.);
  HEfficCentral[2]->SetMaximum(100.);

  HEffic[1]->SetMinimum(5.);
  HEffic[1]->SetMaximum(100.);
  HEffic[2]->SetMinimum(5.);
  HEffic[2]->SetMaximum(100.);

  for (Int_t i = 0; i < nbchannels; i++) {
    c607[i]->cd();
    xframe[i]->Draw();
    t->DrawLatex(0.6, 0.9, tnamesensor[channel[i]]);
    t->DrawLatex(0.6, 0.85, tbuildsensor[channel[i]]);
    t->DrawLatex(0.6, 0.35, tirrsensor[channel[i]]);

    c603[i]->cd();
    HEfficCentral[channel[i]]->Draw("colz");
    t->DrawLatex(0.6, 0.9, tnamesensor[channel[i]]);
    t->DrawLatex(0.6, 0.85, tbuildsensor[channel[i]]);
    t->DrawLatex(0.2, 0.25, tirrsensor[channel[i]]);
    t->DrawLatex(0.2, 0.85, tvaleureffic[channel[i]]);
    t->DrawLatex(0.2, 0.9, "Efficiency (%)");
    //  c603[i]->Print(tnameefficCentralpng[channel[i]]);

    c604[i]->cd();
    HEffic[channel[i]]->Draw("colz");
    t->DrawLatex(0.6, 0.9, tnamesensor[channel[i]]);
    t->DrawLatex(0.6, 0.85, tbuildsensor[channel[i]]);
    t->DrawLatex(0.2, 0.25, tirrsensor[channel[i]]);
    t->DrawLatex(0.2, 0.9, "Efficiency (%)");
    //    c604[i]->Print(tnameefficpng[channel[i]]);

    c605[i]->cd();
    GEfficCharge[channel[i]]->Draw("ALP");
    if (EfficCentre[channel[i]][5] < 40.) {
      t->DrawLatex(0.6, 0.9, tnamesensor[channel[i]]);
      t->DrawLatex(0.6, 0.85, tbuildsensor[channel[i]]);
      t->DrawLatex(0.6, 0.8, tirrsensor[channel[i]]);
    }
    if (EfficCentre[channel[i]][5] >= 40.) {
      t->DrawLatex(0.6, 0.35, tnamesensor[channel[i]]);
      t->DrawLatex(0.6, 0.3, tbuildsensor[channel[i]]);
      t->DrawLatex(0.6, 0.25, tirrsensor[channel[i]]);
    }
    //    c605[i]->Print(tnameefficvschargepng[channel[i]]);

    c606[i]->cd();
    HOccup[channel[i]]->Draw("colz");
    t->DrawLatex(0.6, 0.9, tnamesensor[channel[i]]);
    t->DrawLatex(0.6, 0.85, tbuildsensor[channel[i]]);
    t->DrawLatex(0.2, 0.25, tirrsensor[channel[i]]);
    t->DrawLatex(0.2, 0.9, "Occupancy");
    //    c606[i]->Print(tnameoccuppng[channel[i]]);
  }
}
