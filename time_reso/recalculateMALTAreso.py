import numpy as np
import scipy
import os
import argparse
import fnmatch

from math import *

def readthedata(file, BV, res, reserr):
    with open(file) as f:
        #count = 0
        for line in f:
            a = line.split()[0:3]
            BV.append(float(a[0]))
            res.append(float(a[1]))
            reserr.append(float(a[2]))
            #count += 1
def calculateNewReso(res):
    resolution = []
    resol_calc = lambda x : sqrt((x**2 + 35**2) - 54.8**2)
    for i in range(len(res)):
        resolution.append(resol_calc(res[i]))
    return resolution

def main():
    #plt.rcParams['text.usetex'] = True
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--File", help = "List of files to be used")
    parser.add_argument("-o", "--OutFileName", help = "Show Output")

    args = parser.parse_args()

    BV = []
    res = []
    resNew = []
    reserr = []

    readthedata(args.File, BV, res, reserr)
    resNew = calculateNewReso(res)

    with open(args.OutFileName, "w+") as file:
        for i in range(len(res)):
            # file.write(str(BV[i]) + " " + str(resNew[i]) +  " " + str(reso_error[i]))
            file.write(str(BV[i]) + " " + str(resNew[i]) +  " " + str(reserr[i]) + "\n")
    
if __name__ == "__main__":
    main()
