import ROOT, sys, argparse
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import stats
from scipy.optimize import curve_fit
from math import *
import uproot
from iminuit import cost, Minuit
from pprint import pprint
from iminuit.cost import LeastSquares

def ProvideParams():

    timeAtMaxLower = np.zeros(2)
    BV = np.zeros(3)
    timeAtMaxLower[0] = int(input("Enter timing cut on timeAtMax[0]: "))
    timeAtMaxLower[1] = int(input("Enter timing cut on timeAtMax[1]: "))
    CutOnPulseHeight = int(input("Enter cut on pulseHeight of SiPM "))
    BV[0] = int(input("Eneter the BV for DUT 1: "))
    BV[1] = int(input("Eneter the BV for DUT 2: "))

    return timeAtMaxLower, CutOnPulseHeight, BV


def FitHist(CFDdiff, n):
    Gauss = lambda x, A, mean, sigma : A*np.exp(-0.5*((x-mean)/sigma)**2)
    lsq = lambda A, mean, sigma : (((data_y - Gauss(data_x, A, mean, sigma))**2)/data_yerr**2).sum()
    mu, si = scipy.stats.norm.fit(CFDdiff)
    print("mu: ", mu, " sigma: ", si)
    n_events = np.count_nonzero((CFDdiff.to_numpy() > (mu-si)) & (CFDdiff.to_numpy() < (mu+si)))
    print("N of bins: ", n_events)
    n_bins = int(sqrt(n_events/si)*1) if int(sqrt(n_events/si)*1)>20 else 20
    data_y, bin_edges = np.histogram(CFDdiff.to_numpy(), bins=int(n_bins), range=(mu-si, mu+si))
    data_x = 0.5*(bin_edges[1:] + bin_edges[:-1])
    index = np.where(data_y == data_y.max())
    index = index[0][0] #in case there're 2 or more max
    mean = data_x[index]
    rms = 110 if int(3*si<n_events) else 110

    data_y, bin_edges = np.histogram(CFDdiff.to_numpy(), bins=int(n_bins*1), range=(mean-rms, mean+rms))
    data_yerr = np.sqrt(data_y)
    data_x = 0.5*(bin_edges[1:] + bin_edges[:-1])
    index = np.where(data_y == data_y.max())
    index = index[0][0]
    probableA = data_y.max()
    print("A: ", probableA)
    mean=data_x[index]

    m = Minuit(lsq, A=probableA/2, mean=mean, sigma=100 )
    m.limits["sigma"]=(si/15, 150)
    m.migrad() # finds minimum of least_squares function
    #m.mnprofile("sigma", bound = 2)
    for p in m.params:
        print(repr(p), "\n")
    print(*m.values)
    #print(*m.params)
    print(m.values[2])
    print(m.errors[2])
    plt.hist(CFDdiff, bins=int(5*n_bins), range=(mean-5*rms, mean+5*rms))
    plt.plot(data_x, Gauss(data_x, *m.values), label='signal fit')
    plt.savefig("CFDdiff"+n+".png")
    plt.clf()
    return m.values[2], m.errors[2], n_bins, rms

def SelectTimeCFD(tree, timeAtMaxLower, nbOfChannels, CutOnPulseHeight):

    branches = tree.arrays()
    timeAtMaxUpper = np.zeros(2)
    timeAtMaxUpper = [timeAtMaxLower[0]+2000, timeAtMaxLower[1]+2000]
    print("timeAtMaxUpper: ", timeAtMaxUpper)
    time_diff_DUT1_up = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,0])<timeAtMaxUpper[0]
    time_diff_DUT1_low = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,0])>timeAtMaxLower[0]
    time_diff_DUT2_up = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,1])<timeAtMaxUpper[1]
    time_diff_DUT2_low = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,1])>timeAtMaxLower[1]
    chargecut_DUT1 = branches["charge"][:,0]>2
    chargecut_DUT2 = branches["charge"][:,1]>2
    pulsecut = branches["pulseHeight"][:,2]>CutOnPulseHeight

    sigma = np.zeros(3)
    bins = np.zeros(3)
    rms = np.zeros(3)
    sigmaerror = np.zeros(3)
    for channelNb in range(nbOfChannels):
        for channelNb2 in range(nbOfChannels):
            if(channelNb2<=channelNb): continue
            #histname="DeltaT"+"ch"+str(channelNb)+"_ch"+str(channelNb2)
            #hist=ROOT.TH1F(histname, histname, 4000,-2000,2000)
            if(channelNb2==2):
                if(channelNb==0):
                    selection = time_diff_DUT1_up & time_diff_DUT1_low & chargecut_DUT1 & pulsecut
                if(channelNb==1):
                    selection = time_diff_DUT2_up & time_diff_DUT2_low & chargecut_DUT2 & pulsecut
                CFDdiff = (branches["timeCFD50"][:,channelNb] - branches["timeCFD20"][:,channelNb2])[selection]
            if(channelNb2==1):
                if(channelNb==0):
                    selection = time_diff_DUT1_up & time_diff_DUT1_low & time_diff_DUT2_up & time_diff_DUT2_low & chargecut_DUT1 & chargecut_DUT2 & pulsecut
                CFDdiff = (branches["timeCFD50"][:,channelNb] - branches["timeCFD50"][:,channelNb2])[selection]
            sig=FitHist(CFDdiff,str(channelNb2)+str(channelNb))
            print("Sigma: "+str(channelNb2)+str(channelNb))
            sigma[channelNb2+channelNb-1] = sig[0]
            sigmaerror[channelNb2+channelNb-1] = sig[1]
            bins[channelNb2+channelNb-1] = sig[2]
            rms[channelNb2+channelNb-1] = sig[3]
            print(sigma[channelNb2+channelNb-1])

    resolution = lambda x, y, z : sqrt((x**2 + y**2 - z**2)/2)
    resolution_error = lambda resol, delta1, delta2, delta3, error1, error2, error3 : (1/2)*sqrt( (error1*delta1/resol)**2 + (error2*delta2/resol)**2 + (error3*delta3/resol)**2)
    resolution_batch3 = sqrt(sigma[1]**2-62.80883124**2)
    print(resolution_batch3)

    reso, reso_error = [0,0,0], [0,0,0]
    reso[0] = resolution(sigma[0], sigma[1], sigma[2])
    reso[1] = resolution(sigma[0], sigma[2], sigma[1])
    reso[2] = resolution(sigma[2], sigma[1], sigma[0])

    reso_error[0] = resolution_error(reso[0], sigma[0], sigma[1], sigma[2], sigmaerror[0], sigmaerror[1], sigmaerror[2])
    reso_error[1] = resolution_error(reso[1], sigma[0], sigma[1], sigma[2], sigmaerror[0], sigmaerror[1], sigmaerror[2])
    reso_error[2] = resolution_error(reso[2], sigma[0], sigma[1], sigma[2], sigmaerror[0], sigmaerror[1], sigmaerror[2])

    print("Compute the time resolution")
    for i in range(len(reso)):
        print(i , reso[i], reso_error[i])

    return reso, reso_error, bins, rms

def main():
    print("This script calculates Resolution with 3 channels available")
    nbOfChannels = 3

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--InFileName", help = "Show Input")
    parser.add_argument("-o", "--OutFileName", help = "Show Output")

    args = parser.parse_args()
    inFile = uproot.open(args.InFileName)
    tree = inFile["tree"]

    #outHistFile = ROOT.TFile.Open(outFileName ,"RECREATE")
    if args.InFileName:
        print("Reading from: % s" % args.InFileName)

    timeAtMaxLower, CutOnPulseHeight, BV = ProvideParams()
    reso, reso_error, bins, sigma = SelectTimeCFD(tree, timeAtMaxLower, nbOfChannels, CutOnPulseHeight)

    with open(args.OutFileName, "w+") as file:
        for i in range(len(reso)):
            file.write(str(BV[i]) + " " + str(reso[i]) +  " " + str(reso_error[i]) + " " + str(bins[i]) +  " " + str(sigma[i]) + "\n")


if __name__ == "__main__":
    main()
