import ROOT, sys, argparse
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import stats
from scipy.optimize import curve_fit
from math import *
import uproot
from iminuit import cost, Minuit, describe
from probfit import Extended, BinnedChi2, UnbinnedLH, BinnedLH
from pprint import pprint
from iminuit.cost import LeastSquares
from histograms import histograms

def ProvideParams():

    timeAtMaxLower = np.zeros(2)
    BV = np.zeros(3)
    timeAtMaxLower[0] = 0
    timeAtMaxLower[1] = 0
    CutOnPulseHeight = 6
    BV[0] = int(input("Eneter the BV for DUT 1: "))
    BV[1] = int(input("Eneter the BV for DUT 2: "))

    return timeAtMaxLower, CutOnPulseHeight, BV

def resolution_from_sipm(sigmaCFD20, errorCFD20):

    reso = sqrt(abs(sigmaCFD20**2 - 61.9**2))
    error = sqrt( (errorCFD20*sigmaCFD20/reso)**2 + (61.9/reso)**2)
    return reso, error

def Fit21(CFDdiff, n):
  
    # array = CFDdiff.to_numpy() 
    # mean = np.average(array)
    # mask = (array > mean -200 ) & (array < mean + 200)
    # new_arr = array[mask]
    
    # Gauss = lambda x, mean, sigma : np.exp(-0.5*(x-mean)**2/sigma**2)/(sqrt(2*pi)*sigma)
                                    
    # ulh = UnbinnedLH(Gauss, new_arr)

    # m = Minuit(ulh, mean=mean, sigma=100.)
    # m.set_up(1) #remember up is 0.5 for likelihood and 1 for chi^2
    # m.migrad()

    # ulh.show(m)
    # return m.values[1], m.errors[1]

    array = CFDdiff.to_numpy() 
    mean = np.average(array)    
    data_y, bin_edges = np.histogram(CFDdiff.to_numpy(), bins=100, range=(0,2000))
    data_x = 0.5*(bin_edges[1:] + bin_edges[:-1])
    mean = data_x[np.where(data_y == data_y.max())]
    range_fit = [mean[0]-210, mean[0]+190]
    data_y, bin_edges = np.histogram(CFDdiff.to_numpy(), bins=20, range=(range_fit[0],range_fit[1]))
    data_x = 0.5*(bin_edges[1:] + bin_edges[:-1])
    data_yerr = np.sqrt(data_y)
    Gauss = lambda x, A, mean, sigma : A*np.exp(-0.5*((x-mean)/sigma)**2)

    c = cost.LeastSquares(data_x, data_y, data_yerr, Gauss)
    m = Minuit(c, A=500., mean=mean, sigma=100.0)
    m.migrad()

    figure, ax = plt.subplots()
    ax.hist(CFDdiff, bins=100, range=(0, 2000), histtype="step")
    ax.plot(data_x, Gauss(data_x, m.values[0], m.values[1], m.values[2] ), label='sigma='+ '%.2f' %m.values[2] + ' +- '+ '%.2f' %m.errors[2])
    leg = ax.legend()
    figure.savefig("CFDdiff"+str(n)+".png")
    # plt.show()
    figure.clf()
    return m.values[2], m.errors[2], range_fit[0], range_fit[1]

def Fit20(CFDdiff, n):
    array = CFDdiff.to_numpy() 
    mean = np.average(array)    
    data_y, bin_edges = np.histogram(CFDdiff.to_numpy(), bins=100, range=(0,2000))
    data_x = 0.5*(bin_edges[1:] + bin_edges[:-1])
    mean = data_x[np.where(data_y == data_y.max())]
    range_fit = [mean[0]-170, mean[0]+150]
    data_y, bin_edges = np.histogram(CFDdiff.to_numpy(), bins=16, range=(range_fit[0],range_fit[1]))
    data_x = 0.5*(bin_edges[1:] + bin_edges[:-1])
    data_yerr = np.sqrt(data_y)
    Gauss = lambda x, A, mean, sigma : A*np.exp(-0.5*((x-mean)/sigma)**2)

    c = cost.LeastSquares(data_x, data_y, data_yerr, Gauss)
    m = Minuit(c, A=500., mean=mean, sigma=100.0)
    m.migrad()

    figure, ax = plt.subplots()
    ax.hist(CFDdiff, bins=100, range=(0, 2000), histtype="step")
    ax.plot(data_x, Gauss(data_x, m.values[0], m.values[1], m.values[2] ), label='sigma='+ '%.2f' %m.values[2] + ' +- '+ '%.2f' %m.errors[2])
    leg = ax.legend()
    figure.savefig("CFDdiff"+str(n)+".png")
    # plt.show()
    figure.clf()
    return m.values[2], m.errors[2], range_fit[0], range_fit[1]

def Fit10(CFDdiff, n):
  
    array = CFDdiff.to_numpy() 
    array=np.float64(array)
    # mean = np.average(array)
    # mask = (array >  -200 ) & (array <  + 200)
    # new_arr = array[mask]
    data_y, bin_edges = np.histogram(CFDdiff.to_numpy(), bins=100, range=(0-1000, 0+1000))
    data_x = 0.5*(bin_edges[1:] + bin_edges[:-1])
    mean = data_x[np.where(data_y == data_y.max())]
    print(mean)
    range_fit = [mean[0]-160, mean[0]+160] #charge >4
    data_y, bin_edges = np.histogram(CFDdiff.to_numpy(), bins=16, range=(range_fit[0],range_fit[1]))
    data_x = 0.5*(bin_edges[1:] + bin_edges[:-1])
    data_yerr = np.sqrt(data_y)
    # data_x = (data_x > -200) & (200 > data_x)
    # Gauss = lambda x, mean, sigma : np.exp(-0.5*(x-mean)**2/sigma**2)/(sqrt(2*pi)*sigma)                                  
    Gauss = lambda x, A, mean, sigma : A*np.exp(-0.5*((x-mean)/sigma)**2)

    c = cost.LeastSquares(data_x, data_y, data_yerr, Gauss)
    # we set the signal amplitude to zero and fix all signal parameters
    m = Minuit(c, A =10., mean=mean, sigma=100.0)
    # we temporarily mask out the signal
    m.migrad()

    #it can also do extended one if you pass it an extended pdf and pass extended=True to BinnedLH
    # plt.hist(CFDdiff, bins=200, range=(-1000, 1000), histtype='step')
    figure, ax = plt.subplots()
    ax.hist(CFDdiff, bins=100, range=(-1000, 1000), histtype="step")
    # m.values[2] = 83.6
    # m.errors[2] = 6
    # range_fit[0] = 140
    # range_fit[1] = 280
    ax.plot(data_x, Gauss(data_x, m.values[0], m.values[1], m.values[2] ), label='sigma='+ '%.2f' %m.values[2] + ' +- '+ '%.2f' %m.errors[2])
    leg = ax.legend()
    figure.savefig("CFDdiff"+str(n)+".png")
    # plt.show()
    figure.clf()
    return m.values[2], m.errors[2], range_fit[0], range_fit[1] 
    # return 54.14, 8.9, 110, 250 


def SelectTimeCFD(tree, timeAtMaxLower, nbOfChannels, CutOnPulseHeight):

    branches = tree.arrays()
    timeAtMaxUpper = np.zeros(2)
    timeAtMaxUpper = [timeAtMaxLower[0]+2000, timeAtMaxLower[1]+2000]
    print("timeAtMaxUpper: ", timeAtMaxUpper)
    time_diff_DUT1_up = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,0])<timeAtMaxUpper[0]
    time_diff_DUT1_low = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,0])>timeAtMaxLower[0]
    time_diff_DUT2_up = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,1])<timeAtMaxUpper[1]
    time_diff_DUT2_low = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,1])>timeAtMaxLower[1]
    # geometry_DUT1y = abs((branches["Ytr"][:,0])-117)<13.5
    # geometry_DUT1x = abs((branches["Xtr"][:,0])-625)<13.5
    # geometry_DUT2y = abs((branches["Ytr"][:,1])-139)<13.5 
    # geometry_DUT2x = abs((branches["Xtr"][:,1])-635)<13.5
    chargecut_DUT1 = branches["charge"][:,0]>2
    chargecut_DUT2 = branches["charge"][:,1]>2
    pulsecut = branches["pulseHeight"][:,2]>CutOnPulseHeight

    sigma = np.zeros(3)
    sigmaerror = np.zeros(3)
    fit_start = np.zeros(3)
    fit_end = np.zeros(3)

    for channelNb in range(nbOfChannels):
        for channelNb2 in range(nbOfChannels): 
            if(channelNb2<=channelNb): continue  #01 02 12
            #histname="DeltaT"+"ch"+str(channelNb)+"_ch"+str(channelNb2)
            #hist=ROOT.TH1F(histname, histname, 4000,-2000,2000)
            if(channelNb2==2):
                if(channelNb==0):
                    selection = time_diff_DUT1_up & time_diff_DUT1_low & chargecut_DUT1 & pulsecut # & geometry_DUT1x & geometry_DUT1y
                if(channelNb==1):
                    selection = time_diff_DUT2_up & time_diff_DUT2_low  & pulsecut  & chargecut_DUT2 # & geometry_DUT2y & geometry_DUT2x
                CFDdiff = ( branches["timeCFD20" ][:,channelNb2] - branches["timeCFD50"][:,channelNb])[selection]
                if channelNb==0:
                        sig=Fit20(CFDdiff, channelNb2+channelNb)
                if channelNb==1:
                        sig=Fit21(CFDdiff, channelNb2+channelNb)
            if(channelNb2==1):
                if(channelNb==0):
                    selection = time_diff_DUT1_up & time_diff_DUT1_low & time_diff_DUT2_up & time_diff_DUT2_low & chargecut_DUT1 & chargecut_DUT2 & pulsecut #& geometry_DUT1x & geometry_DUT1y & geometry_DUT2x & geometry_DUT2y
                CFDdiff = (branches["timeCFD50"][:,channelNb] - branches["timeCFD50"][:,channelNb2])[selection]
                sig=Fit10(CFDdiff, channelNb2+channelNb)
            sigma[channelNb2+channelNb-1] = sig[0]
            sigmaerror[channelNb2+channelNb-1] = sig[1]
            fit_start[[channelNb2+channelNb-1]] = sig[2]
            fit_end[[channelNb2+channelNb-1]] = sig[3]

    reso, reso_error = [0,0,0], [0,0,0]

    #################### Resolution with 3 devices ####################

    resolution = lambda x, y, z : sqrt(abs(x**2 + y**2 - z**2)/2)
    resolution_error = lambda resol, delta1, delta2, delta3, error1, error2, error3 : sqrt( (error1*delta1/resol)**2 + (error2*delta2/resol)**2 + (error3*delta3/resol)**2)

    reso[0] = resolution(sigma[0], sigma[1], sigma[2])
    reso[1] = resolution(sigma[0], sigma[2], sigma[1])
    reso[2] = resolution(sigma[2], sigma[1], sigma[0])

    reso_error[0] = resolution_error(reso[0], sigma[0], sigma[1], sigma[2], sigmaerror[0], sigmaerror[1], sigmaerror[2])
    reso_error[1] = resolution_error(reso[1], sigma[0], sigma[1], sigma[2], sigmaerror[0], sigmaerror[1], sigmaerror[2])
    reso_error[2] = resolution_error(reso[2], sigma[0], sigma[1], sigma[2], sigmaerror[0], sigmaerror[1], sigmaerror[2])

    #################### Resolution with 2 devices ####################

    # reso[0], reso_error[0] = resolution_from_sipm(sigma[1], sigmaerror[1])

    
    print("Compute the time resolution")
    for i in range(len(reso)):
        print(i , reso[i], reso_error[i])

    return reso, reso_error, fit_start, fit_end

def main():
    print("This script calculates Resolution with 3 channels available")
    nbOfChannels = 3

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--InFileName", help = "Show Input")
    parser.add_argument("-o", "--OutFileName", help = "Show Output")

    args = parser.parse_args()
    inFile = uproot.open(args.InFileName)
    tree = inFile["tree"]

    #outHistFile = ROOT.TFile.Open(outFileName ,"RECREATE")
    if args.InFileName:
        print("Reading from: % s" % args.InFileName)

    timeAtMaxLower, CutOnPulseHeight, BV = ProvideParams()
    reso, reso_error, fit_start, fit_end = SelectTimeCFD(tree, timeAtMaxLower, nbOfChannels, CutOnPulseHeight)
    with open(args.OutFileName, "w+") as file:
        for i in range(len(reso)):
            file.write(str(BV[i]) + " " + str(reso[i]) +  " " + str(reso_error[i]) + " " + str(fit_start[i]) +  " " + str(fit_end[i]) + "\n")


if __name__ == "__main__":
    main()
