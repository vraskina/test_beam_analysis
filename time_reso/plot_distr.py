import ROOT, sys, argparse
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import stats
from scipy.optimize import curve_fit
from math import *
import uproot


def SelectTimeCFD(tree, CutOnPulseHeight):
    branches = tree.arrays()
    chargecut_DUT = [branches["charge"][:,0]>2, branches["charge"][:,1]>2]
    pulsecut = branches["pulseHeight"][:,2]>CutOnPulseHeight

    timeCFD=[[],[], []]
    
    for channelNb in range(2):       
        time_diff_DUT_up = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,channelNb])<2000
        time_diff_DUT_low = (branches["timeCFD20"][:,2]-branches["timeCFD50"][:,channelNb])>0             
        selection = pulsecut & chargecut_DUT[channelNb]
        # timeCFD[channelNb] = ( branches["charge"][:,channelNb] )[selection]
        timeCFD[channelNb] = (branches["timeCFD20"][:,2] - branches["timeCFD50"][:,channelNb])[selection]
    selection = pulsecut & chargecut_DUT[0] & chargecut_DUT[1]
    timeCFD[2] = (branches["timeCFD50"][:,0] - branches["timeCFD50"][:,1])[selection]
  
    return timeCFD


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--File",nargs='+', help = "List of files to be used")

    args = parser.parse_args()

    list_of_files = args.File
    n_of_files = len(list_of_files)
    figure, axis = plt.subplots(2, 3)
    CutOnPulseHeight = int(input("Enter cut on pulseHeight of SiPM "))
    num_file = 0
    for file in list_of_files:
        inFile = uproot.open(file)
        tree = inFile["tree"]
        CFDfin = SelectTimeCFD(tree, CutOnPulseHeight)
        if num_file==0:
            version = "New"
        if num_file==1:
            version = "Old"

        axis[num_file, 0].hist(CFDfin[0], bins=200, range=(0, 2000), label= version)
        axis[num_file, 0].text(0.01, 0.55, "timeCFD50[0]-timeCFD20[2]", transform=axis[num_file, 0].transAxes)
        # axis[num_file, 0].text(0.6, 0.55, "charge[0]", transform=axis[num_file, 0].transAxes)    
        axis[num_file, 0].text(0.6, 0.35, "N events " + str(len(CFDfin[0])), transform=axis[num_file, 0].transAxes)     
        leg = axis[num_file, 0].legend()
        axis[num_file, 1].hist(CFDfin[1], bins=200, range=(0, 2000), label=version)
        axis[num_file, 1].text(0.01, 0.55, "timeCFD50[1]-timeCFD20[2]", transform=axis[num_file, 1].transAxes) 
        # axis[num_file, 1].text(0.6, 0.55, "charge[1]", transform=axis[num_file, 1].transAxes)         
        axis[num_file, 1].text(0.6, 0.35, "N events " + str(len(CFDfin[1])), transform=axis[num_file, 1].transAxes)     
        leg = axis[num_file, 1].legend()
        axis[num_file, 2].hist(CFDfin[2], bins=200, range=(-1000, 1000), label=version)
        axis[num_file, 2].text(0.01, 0.55, "timeCFD50[1]-timeCFD50[0]", transform=axis[num_file, 2].transAxes)    
        # axis[num_file, 2].text(0.6, 0.55, "pulseHeight[2]", transform=axis[num_file, 2].transAxes)    
        axis[num_file, 2].text(0.6, 0.35, "N events " + str(len(CFDfin[2])), transform=axis[num_file, 2].transAxes)     
        leg = axis[num_file, 2].legend()

        num_file = num_file + 1
    plt.show()

    #outHistFile = ROOT.TFile.Open(outFileName ,"RECREATE")

if __name__ == "__main__":
    main()


