//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul 20 12:27:24 2020 by ROOT version 6.22/00
// from TTree tree/Test beam
// found on file: tree_September2018_305_004453.root
//////////////////////////////////////////////////////////

#ifndef Runanalyse_h
#define Runanalyse_h
#include "Riostream.h"
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>


// Header file for the classes stored in the TTree if any.

class Runanalyse {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   Int_t           batchNum ;

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           FEI4x;
   Int_t           FEI4y;
   Int_t           runNumber;
   Int_t           eventNumber;
   Int_t           cycleNumber;
   Float_t         eventTime;
   Int_t           nChannel;
   Float_t         pulseHeightNoise[8];   //[nChannel]
   Float_t         pulseHeight[8];   //[nChannel]
   Float_t         timeCFD[8];   //[nChannel]
   Float_t         timeZCD[8];   //[nChannel]
   Float_t         timeCFD20[8];   //[nChannel]
   Float_t         timeCFD70[8];   //[nChannel]
   Float_t         timeZCD20[8];   //[nChannel]
   Float_t         timeCFD50[8];   //[nChannel]
   Float_t         timeZCD50[8];   //[nChannel]
   Float_t         timeCTD[8];   //[nChannel]
   Float_t         timeAtMax[8];   //[nChannel]
   Float_t         pedestal[8];   //[nChannel]
   Float_t         noise[8];   //[nChannel]
   Float_t         noiseZCD[8];   //[nChannel]
   Float_t         charge[8];   //[nChannel]
   Float_t         jitter[8];   //[nChannel]
   Float_t         jitterCFD20[8];   //[nChannel]
   Float_t         jitterCFD50[8];   //[nChannel]
   Float_t         jitterCTD[8];   //[nChannel]
   Float_t         riseTime1090[8];   //[nChannel]
   Float_t         derivative[8];   //[nChannel]
   Float_t         derivativeCFD20[8];   //[nChannel]
   Float_t         derivativeCFD50[8];   //[nChannel]
   Float_t         derivativeCTD[8];   //[nChannel]
   Float_t         derivativeZCD[8];   //[nChannel]
   Float_t         width[8];   //[nChannel]
   Float_t         TOT[8];   //[nChannel]
   Double_t        Xtr[8];
   Double_t        Ytr[8];

   // List of branches
   TBranch        *b_FEI4x;   //!
   TBranch        *b_FEI4y;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_cycleNumber;   //!
   TBranch        *b_eventTime;   //!
   TBranch        *b_nChannel;   //!
   TBranch        *b_pulseHeightNoise;   //!
   TBranch        *b_pulseHeight;   //!
   TBranch        *b_timeCFD;   //!
   TBranch        *b_timeZCD;   //!
   TBranch        *b_timeCFD20;   //!
   TBranch        *b_timeCFD70;   //!
   TBranch        *b_timeZCD20;   //!
   TBranch        *b_timeCFD50;   //!
   TBranch        *b_timeZCD50;   //!
   TBranch        *b_timeCTD;   //!
   TBranch        *b_timeAtMax;   //!
   TBranch        *b_pedestal;   //!
   TBranch        *b_noise;   //!
   TBranch        *b_noiseZCD;   //!
   TBranch        *b_charge;   //!
   TBranch        *b_jitter;   //!
   TBranch        *b_jitterCFD20;   //!
   TBranch        *b_jitterCFD50;   //!
   TBranch        *b_jitterCTD;   //!
   TBranch        *b_riseTime1090;   //!
   TBranch        *b_derivative;   //!
   TBranch        *b_derivativeCFD20;   //!
   TBranch        *b_derivativeCFD50;   //!
   TBranch        *b_derivativeCTD;   //!
   TBranch        *b_derivativeZCD;   //!
   TBranch        *b_width;   //!
   TBranch        *b_TOT;   //!
   TBranch        *b_Xtr;   //!
   TBranch        *b_Ytr;   //!

   Runanalyse(TTree *tree=0, Int_t batchnumber=-1);
   virtual ~Runanalyse();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Runanalyse_cxx
Runanalyse::Runanalyse(TTree *tree, Int_t batchnumber) : fChain(0)
{
  std::string rootOutputName = "../run/Run_batch";
  std::string endOutputName = ".root";

  std::string outputName;

  outputName = rootOutputName + std::to_string(batchnumber) + endOutputName;

  batchNum = batchnumber; // Je donne le no de batch que je vais utiliser dans le .C
  cout << "on tourne sur " << outputName.c_str() << endl;
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
     TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(outputName.c_str());
      if (!f || !f->IsOpen()) {
	f = new TFile(outputName.c_str());
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

Runanalyse::~Runanalyse()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Runanalyse::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Runanalyse::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Runanalyse::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("FEI4x", &FEI4x, &b_FEI4x);
   fChain->SetBranchAddress("FEI4y", &FEI4y, &b_FEI4y);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("cycleNumber", &cycleNumber, &b_cycleNumber);
   fChain->SetBranchAddress("eventTime", &eventTime, &b_eventTime);
   fChain->SetBranchAddress("nChannel", &nChannel, &b_nChannel);
   fChain->SetBranchAddress("pulseHeightNoise", pulseHeightNoise, &b_pulseHeightNoise);
   fChain->SetBranchAddress("pulseHeight", pulseHeight, &b_pulseHeight);
   fChain->SetBranchAddress("timeCFD", timeCFD, &b_timeCFD);
   fChain->SetBranchAddress("timeZCD", timeZCD, &b_timeZCD);
   fChain->SetBranchAddress("timeCFD20", timeCFD20, &b_timeCFD20);
   fChain->SetBranchAddress("timeCFD70", timeCFD70, &b_timeCFD70);
   fChain->SetBranchAddress("timeZCD20", timeZCD20, &b_timeZCD20);
   fChain->SetBranchAddress("timeCFD50", timeCFD50, &b_timeCFD50);
   fChain->SetBranchAddress("timeZCD50", timeZCD50, &b_timeZCD50);
   fChain->SetBranchAddress("timeCTD", timeCTD, &b_timeCTD);
   fChain->SetBranchAddress("timeAtMax", timeAtMax, &b_timeAtMax);
   fChain->SetBranchAddress("pedestal", pedestal, &b_pedestal);
   fChain->SetBranchAddress("noise", noise, &b_noise);
   fChain->SetBranchAddress("noiseZCD", noiseZCD, &b_noiseZCD);
   fChain->SetBranchAddress("charge", charge, &b_charge);
   fChain->SetBranchAddress("jitter", jitter, &b_jitter);
   fChain->SetBranchAddress("jitterCFD20", jitterCFD20, &b_jitterCFD20);
   fChain->SetBranchAddress("jitterCFD50", jitterCFD50, &b_jitterCFD50);
   fChain->SetBranchAddress("jitterCTD", jitterCTD, &b_jitterCTD);
   fChain->SetBranchAddress("riseTime1090", riseTime1090, &b_riseTime1090);
   fChain->SetBranchAddress("derivative", derivative, &b_derivative);
   fChain->SetBranchAddress("derivativeCFD20", derivativeCFD20, &b_derivativeCFD20);
   fChain->SetBranchAddress("derivativeCFD50", derivativeCFD50, &b_derivativeCFD50);
   fChain->SetBranchAddress("derivativeCTD", derivativeCTD, &b_derivativeCTD);
   fChain->SetBranchAddress("derivativeZCD", derivativeZCD, &b_derivativeZCD);
   fChain->SetBranchAddress("width", width, &b_width);
   fChain->SetBranchAddress("TOT", TOT, &b_TOT);
   fChain->SetBranchAddress("Xtr", Xtr, &b_Xtr);
   fChain->SetBranchAddress("Ytr", Ytr, &b_Ytr);
   Notify();
}

Bool_t Runanalyse::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Runanalyse::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Runanalyse::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Runanalyse_cxx
