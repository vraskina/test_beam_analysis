import matplotlib.pyplot as plt
import matplotlib.markers as mrk
plt.rcParams['figure.figsize'] = [10, 10]
import os
import argparse
import fnmatch
import numpy as np
import mplhep as hep
plt.style.use(hep.style.ROOT)

def readthedata(file, BV, res, reserr):
    with open(file) as f:
        #count = 0
        for line in f:
            a = line.split()[0:3]
            BV.append(float(a[0]))
            res.append(float(a[1]))
            reserr.append(float(a[2]))
            #count += 1

def readthedata_double_error(file, BV, res, reserr):
    with open(file) as f:
        for line in f:
            a = line.split()[0:3]
            BV.append(float(a[0]))
            res.append(float(a[1]))
            reserr.append(2*float(a[2]))

def main():
    #plt.rcParams['text.usetex'] = True
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--File",nargs='+', help = "List of files to be used")
    parser.add_argument("-s", "--Sensors", nargs='+', help = "List of sensors' names")
    parser.add_argument("-t", "--TBCampaign", nargs='+', help = "Test beam campaign name (ex. DESY)")
    parser.add_argument("-y", "--year", nargs='+', help = "Test beam campaign year (ex. 2022)")
    parser.add_argument("-c", "--temp", nargs='+', help = "Show the temperature")
    args = parser.parse_args()
    list_of_files = args.File
    name_sensor = args.Sensors
    TB = args.TBCampaign
    year = args.year
    temperature = args.temp
    irradiation = input("Enter the irradiation of sensor ")
    textstr = '$\phi_{eq}$ = '+ str(irradiation) + '$x10^{15}n_{eq}cm^{-2}$'

    colors = ['#0485d1','#040273','#e50000', '#8c000F' ,'#00FF00'] #for 1.5
    markers = ['s', 'd', '*', 'X', 'o'] #for 1.5

    # colors = ['#0485d1','#040273','#8c000F' ,'#00FF00'] #for 2.5
    # markers = ['s', 'd', 'X', 'o'] #for 2.5

    fig_res, ax_res = plt.subplots()
    malta_range = [1,3]
    count = 0
    for file in list_of_files:
        BV = []
        res = []
        reserr = []
        if count in malta_range:
            readthedata(file, BV, res, reserr)
        else :
            readthedata_double_error(file, BV, res, reserr)
        ax_res.text(0.1, 0.06, textstr, transform=ax_res.transAxes)
        #plt.plot(BV,res, marker = '.', linestyle='None')
        ax_res.errorbar(BV,res, yerr = reserr, marker = markers[count],  linestyle='--', markersize=12, color = colors[count], label=name_sensor[count]+ " (" + temperature[count] + "$^{o}$C, " + TB[count]+" "+year[count]+")")
        #fig_res.suptitle(name_sensor, fontsize=18)

        count+= 1
    hep.atlas.text(' Preliminary Test Beam')
    plt.ylim(25, 100)
    plt.xlim(170, 550)
    plt.xlabel('Bias Voltage[V]')
    plt.ylabel('Time resolution [ps]')
    legend = ax_res.legend(loc = 'upper left',fontsize=16)
    plt.hlines(70.0,170, 600,color='black',linestyle='dotted')

    fig_res.savefig('res_irradiation_%s'%irradiation+'.png')
    fig_res.savefig('res_irradiation_%s'%irradiation+'.pdf')
    plt.show()
    plt.clf()

if __name__ == "__main__":
    main()
