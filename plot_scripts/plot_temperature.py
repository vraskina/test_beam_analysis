import matplotlib.pyplot as plt
import matplotlib.markers as mrk
import os
import argparse
import fnmatch
import numpy as np
import mplhep as hep
plt.style.use(hep.style.ROOT)

def readthedata(file, T, MPV, eff):
    with open(file) as f:
        #count = 0
        for line in f:
            a = line.split()[0:3]
            T.append(float(a[0]))
            #count += 1


def main():
    #plt.rcParams['text.usetex'] = True
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--File",nargs='+', help = "List of files to be used")
    parser.add_argument("-i", "--Iradiation", nargs='+', help = "List of sensors' iradiations")
    args = parser.parse_args()
    list_of_files = args.File
    irradiation = args.Iradiation
    sensor = input("Enter the sensor name")
    textstr = str(sensor)
    #textstr = str(irradiation)
    # colors = ['#00008B','#15B01A','#C20078', 'maroon' ,'#E50000']
    # #colors = ['tab:green','tab:purple','tab:red', 'tab:pink', '#E50000']
    # markers = ['o', 'd', '*', 'X']

    # colors = ['tab:green', '#0485d1', 'tab:red', 'tab:purple', 'tab:pink', ]
    # markers = ['o', 's',  '*', 'd', 'X']

    colors = ['#00008B','#C20078','#15B01A', 'maroon' ,'#E50000']
    markers = ['o', 'd', '*', 'X']
    # colors = ['#C20078','#15B01A', 'maroon' ,'#E50000']
    # markers = ['d', '*', 'X']

    fig_mpv, ax_mpv = plt.subplots()
    count = 0
    for file in list_of_files:
        T = []
        MPV = []
        eff = []
        readthedata(file, T, MPV, eff)
        run = [*range(0, len(T), 1)]
        ax_mpv.text(0.05, 0.05, textstr, transform=ax_mpv.transAxes)
        #plt.plot(T,MPV, marker = '.', linestyle='None')
        plt.ylim(-20,-45)
        plt.xlim(0,70)
        ax_mpv.plot(run,T, marker = markers[count],  linestyle='--', markersize=7, color = colors[count], label= "$\phi_{eq}$ =" + "%s"%irradiation[count] + '$n_{eq}cm^{-2}$')
        #fig_mpv.suptitle(irradiation, fontsize=18)
        plt.xlabel('Run[#]')
        plt.hlines(4.0,80,600,color='red',linestyle='dotted')
        plt.ylabel('Temperature [$^{o}$C]')
        count+= 1
    hep.atlas.text(' Preliminary Test Beam')
    legend = ax_mpv.legend(loc = 'upper right',fontsize=16)
    #fig_mpv.savefig('MPV_irradiation_%s'%irradiation+'.png')
    fig_mpv.savefig('Temperatures_%s'%sensor+'.pdf')

    plt.show()
    plt.clf()

if __name__ == "__main__":
    main()
