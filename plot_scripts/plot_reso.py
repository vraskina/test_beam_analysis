import matplotlib.pyplot as plt
import matplotlib.markers as mrk
import os
import argparse
import fnmatch
import numpy as np

def plot(x,y,yerr, n_of_batch, name_sensor, temperature, irradiation):
    for i in range(0,3):
        fig, ax = plt.subplots()
        plt.errorbar(x[i],y[i], yerr = yerr[i], marker = '.', markersize=12, linestyle='None')
        textstr = '\n'.join((
            r'T['+u"\N{DEGREE SIGN}"+'] = '+ ', '.join(temperature[i]),
            r'Irradiation = '+ str(irradiation[i])))
        ax.text(0.1, 0.87, textstr, transform=ax.transAxes, fontsize=12)
        plt.ylim(30,120)
        #fig.suptitle('test title', fontsize=20)
        fig.suptitle(name_sensor[i], fontsize=18)
        plt.xlabel('Bias Voltage[V]', fontsize=15)
        plt.ylabel('Resolution [ps]', fontsize=15)
        fig.savefig('resolution_batch_%s'%n_of_batch+'_'+str(i)+'.jpg')
        plt.show()


def readthedata(file, BV, res, reserr):
    with open(file) as f:
        count = 0
        for line in f:
            a = line.split()[0:3]
            BV[count].append(float(a[0]))
            res[count].append(float(a[1]))
            reserr[count].append(float(a[2]))
            count += 1
            #plt.plot(x,y, "ro")
            #plt.axis([0, 6, 0, 20])
            #print(x, y)


def scan_folder(path, BV, res, reserr):
    # iterate over all the files in directory 'path'
    for file in os.listdir(path):
        if fnmatch.fnmatch(file,'run*.txt'):
            fullname = os.path.join(path,file)
            print(fullname)
            readthedata(fullname, BV, res, reserr)
        else:
            current_path = "".join((path, "/", file))
            if os.path.isdir(current_path):
                # if we're checking a sub-directory, recursively call this method
               scan_folder(current_path, BV, res, reserr)
    return BV, res, reserr

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--NBatch", help = "Show number of batch")
    parser.add_argument("-t1", "--Temperature1", nargs='+', type=str, help = "Show temperature points for DUT1")
    parser.add_argument("-t2", "--Temperature2", nargs='+', type=str, help = "Show temperature points for DUT2")
    args = parser.parse_args()
    BV = [[],[],[]]
    res = [[],[],[]]
    reserr = [[],[],[]]
    Temperature = [args.Temperature1, args.Temperature2,['Room temperature']]
    scan_folder("../batch%s"%args.NBatch+"xx", BV, res, reserr)
    name_sensor = [[],[],'SiPM']
    name_sensor[0] = input("Enter the name of sensor 1: ")
    name_sensor[1] = input("Enter the name of sensor 2: ")
    irradiation = [[],[],[0]]
    irradiation[0] = input("Enter the irradiation of sensor 1: ")
    irradiation[1] = input("Enter the irradiation of sensor 2: ")
    plot(BV, res, reserr, args.NBatch, name_sensor, Temperature, irradiation)
    #plt.axis([400, 600, 30, 70])

if __name__ == "__main__":
    main()
