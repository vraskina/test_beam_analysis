import matplotlib.pyplot as plt
import matplotlib.markers as mrk
import os
import argparse
import fnmatch
import numpy as np
from statistics import mean
import mplhep as hep
plt.style.use(hep.style.ROOT)

def readthedata(file, line, BV, res, reserr):
    with open(file) as f:
        lines = f.readlines()
        line_index = lines[int(line)]
        a = line_index.split()[0:3]
        BV.append(float(a[0]))
        res.append(float(a[1]))
        reserr.append(float(a[2]))

#Function works with MALTA data I didn;t analyse
def read_one_file(file):
    bv = []
    reso = []
    with open(file) as f:
        for line in f:
            a = line.split()[0:2]
            bv.append(float(a[0]))
            reso.append(float(a[1]))
    return bv, reso

def scan_folder(path, line, BV, res, reserr):
    # iterate over all the files in directory 'path'
    for file in os.listdir(path):
        if fnmatch.fnmatch(file,'run*.txt'):
            fullname = os.path.join(path,file)
            print(fullname)
            readthedata(fullname, line, BV,  res, reserr)
        else:
            current_path = "".join((path, "/", file))
            if os.path.isdir(current_path):
                # if we're checking a sub-directory, recursively call this method
               scan_folder(current_path, line, BV, res, reserr)
    return res, reserr

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--NBatch", nargs='+', help = "Show batch numbers")
    parser.add_argument("-l", "--NLines", nargs='+', help = "Show batch lines")
    parser.add_argument("-s", "--Nsensors", nargs='+', help = "List of sensors names")
    parser.add_argument("-t", "--TBCampaign", nargs='+', help = "Test beam campaign name (ex. DESY)")
    parser.add_argument("-y", "--year", nargs='+', help = "Show the test beam campaign year")
    parser.add_argument("-c", "--temp", nargs='+', help = "Show the temperature")

    args = parser.parse_args()
    list_of_batches = args.NBatch
    lines = args.NLines
    sensors_name = args.Nsensors
    TB = args.TBCampaign
    year = args.year
    temperature = args.temp
    irradiation = input("Enter the irradiation of sensors ")
    textstr = 'Irradiation = '+ str(irradiation) + '$x10^{15}n_{eq}cm^{-2}$' # If you want sensors with the same irradiation
    #textstr = str(irradiation)  # If you want the sensor with different irradiations
    fig, ax = plt.subplots()
    plt.xlabel('Bias Voltage [V]')
    plt.ylabel('Time resolution [ps]')
    #colors = ['#00008B','#15B01A','#C20078', 'maroon' ,'#E50000']
    colors = ['tab:green','tab:purple','tab:red', 'tab:pink', '#E50000']
    markers = ['o', 'd', '*', 'X']
    count = 0
    for batch in list_of_batches:
        BV = []
        res = []
        reserr = []
        scan_folder("../batch%s"%batch+"xx", lines[count], BV, res, reserr)
        print("Mean resolution for batch " + str(batch) + "xx: ", mean(res))
        ax.text(0.1, 0.15, textstr, transform=ax.transAxes)
        ax.errorbar(BV, res, yerr = reserr, marker = markers[count], markersize=12, color = colors[count], linestyle='--', label=sensors_name[count]+ " (" + temperature[count] + "$^{o}$C, " + TB[count]+" "+year[count]+")")
        count +=1
    bv, reso = read_one_file("/eos/user/v/vraskina/Analysis_Test_Beam/Malta/IMEv2_reso.txt")
    ax.errorbar(bv, reso, marker = markers[count], markersize=12, color = colors[count], linestyle='--', label=sensors_name[count] + " ("+ temperature[count] + "$^{o}$C, " + TB[count]+" "+year[count]+")")
    legend = ax.legend(fontsize=18)
    #hep.atlas.label("ATLAS HGTD", data=False)
    hep.atlas.text(' Preliminary Test Beam')
    plt.ylim(30, 100)
    plt.xlim(170, 570)
    fig.suptitle('')
    #fig.savefig('resolution_irradiation_%s'%irradiation+'.jpg')
    plt.show()
    #plt.axis([400, 600, 30, 70])

if __name__ == "__main__":
    main()
