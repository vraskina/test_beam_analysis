import matplotlib.pyplot as plt
import matplotlib.markers as mrk
import os
import argparse
import fnmatch
import numpy as np
import mplhep as hep
plt.style.use(hep.style.ROOT)

def readthedata(file, BV, MPV, eff):
    with open(file) as f:
        for line in f:
            a = line.split()[0:3]
            BV.append(float(a[0]))
            eff.append(float(a[1]))
            MPV.append(float(a[2]))


def main():
    #plt.rcParams['text.usetex'] = True
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--File",nargs='+', help = "List of files to be used")
    parser.add_argument("-s", "--Sensors", nargs='+', help = "List of sensors' names")
    parser.add_argument("-t", "--TBCampaign", nargs='+', help = "Test beam campaign name (ex. DESY)")
    parser.add_argument("-y", "--year", nargs='+', help = "Test beam campaign year (ex. 2022)")
    parser.add_argument("-c", "--temp", nargs='+', help = "Show the temperature")
    args = parser.parse_args()
    list_of_files = args.File
    name_sensor = args.Sensors
    TB = args.TBCampaign
    year = args.year
    temperature = args.temp
    irradiation = input("Enter the irradiation of sensor ")
    textstr = '$\phi_{eq}$ = '+ str(irradiation) + '$x10^{15}n_{eq}cm^{-2}$'
    colors = ['#0485d1','#040273','#e50000', '#8c000F' ,'#00FF00'] #for 1.5
    markers = ['s', 'd', '*', 'X', 'o'] #for 1.5

    # colors = ['#0485d1','#040273','#8c000F' ,'#00FF00'] #for 2.5
    # markers = ['s', 'd', 'X', 'o'] #for 2.5


    fig_mpv, ax_mpv = plt.subplots()
    count = 0
    for file in list_of_files:
        BV = []
        MPV = []
        eff = []
        readthedata(file, BV, MPV, eff)
        ax_mpv.text(0.05, 0.55, textstr, transform=ax_mpv.transAxes)
        #plt.plot(BV,MPV, marker = '.', linestyle='None')
        plt.ylim(0,14)
        plt.xlim(180,580)
        ax_mpv.plot(BV,MPV, marker = markers[count],  linestyle='--', markersize=12, color = colors[count], label=name_sensor[count]+ " (" + temperature[count] + "$^{o}$C, " + TB[count]+" "+year[count]+")")
        #fig_mpv.suptitle(name_sensor, fontsize=18)
        plt.xlabel('Bias Voltage[V]')
        plt.ylabel('Charge$_{\,\,MPV}$ [fC]')
        count+= 1
    hep.atlas.text(' Preliminary Test Beam')
    legend = ax_mpv.legend(loc = 'upper left',fontsize=16)
    #fig_mpv.savefig('MPV_irradiation_%s'%irradiation+'.png')
    plt.show()
    plt.clf()

    count = 0
    fig_eff, ax_eff = plt.subplots()
    for file in list_of_files:
        BV = []
        MPV = []
        eff = []
        readthedata(file, BV, MPV, eff)
        ax_eff.text(0.35, 0.05, textstr, transform=ax_eff.transAxes)
        #plt.plot(BV,eff, marker = '.', linestyle='None')
        ax_eff.plot(BV,eff, marker = markers[count],  linestyle='--', markersize=12, color = colors[count], label=name_sensor[count]+ " (" + temperature[count] + "$^{o}$C, " + TB[count]+" "+year[count]+")")
        plt.xlim(180,580)
        plt.ylim(30,130)
        #fig.suptitle('test title', fontsize=20)
        #fig_eff.suptitle(name_sensor, fontsize=18)
        plt.xlabel('Bias Voltage[V]')
        plt.ylabel('Efficiency [%]')
        count+= 1
    hep.atlas.text(' Preliminary Test Beam')
    legend = ax_eff.legend(loc = 'upper left', fontsize=16)
    #fig_eff.savefig('eff_irradiation_%s'%irradiation+'.png')
    plt.show()
    plt.clf()
    #plt.axis([400, 600, 30, 70])

if __name__ == "__main__":
    main()
