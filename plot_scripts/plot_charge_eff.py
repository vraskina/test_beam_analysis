import matplotlib.pyplot as plt
import matplotlib.markers as mrk
import os
import argparse
import fnmatch
import numpy as np

def plot_charge(x,y, name_sensor, temperature, irradiation):
    fig, ax = plt.subplots()
    textstr = '\n'.join((
        r'T['+u"\N{DEGREE SIGN}"+'] = '+ ', '.join([str(i) for i in temperature]),
        r'Irradiation = '+ str(irradiation)))
    ax.text(0.10, 0.87, textstr, transform=ax.transAxes, fontsize=12)
    plt.plot(x,y, marker = '.', markersize=12, linestyle='None')
    plt.ylim(2,28)
        #fig.suptitle('test title', fontsize=20)
    fig.suptitle(name_sensor, fontsize=18)
    plt.xlabel('Bias Voltage[V]', fontsize=15)
    plt.ylabel('Charge [fC]', fontsize=15)
    fig.savefig('MPV_sensor_%s'%name_sensor+'.jpg')
    plt.show()

def plot_eff(x,y, name_sensor, temperature, irradiation):
    fig, ax = plt.subplots()
    textstr = '\n'.join((
        r'T['+u"\N{DEGREE SIGN}"+'] = '+ ', '.join([str(i) for i in temperature]),
        r'Irradiation = '+ str(irradiation)))
    ax.text(0.10, 0.87, textstr, transform=ax.transAxes, fontsize=12)
    plt.plot(x,y, marker = '.', markersize=12, linestyle='None')
    plt.ylim(80,110)
        #fig.suptitle('test title', fontsize=20)
    fig.suptitle(name_sensor, fontsize=18)
    plt.xlabel('Bias Voltage[V]', fontsize=15)
    plt.ylabel('Efficiency [%]', fontsize=15)
    fig.savefig('eff_sensor_%s'%name_sensor+'.jpg')
    plt.show()

def readthedata(file, BV, MPV, eff):
    with open(file) as f:
        count = 0
        for line in f:
            a = line.split()[0:3]
            BV.append(float(a[0]))
            eff.append(float(a[1]))
            MPV.append(float(a[2]))
            count += 1


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--File", help = "Show file")
    parser.add_argument("-t", "--Temperature", nargs='+', help = "Show temperature points")
    args = parser.parse_args()
    #list_of_files = args.File
    BV = []
    MPV = []
    eff = []
    irradiation = input("Enter the irradiation of sensor ")
    readthedata("%s"%args.File, BV, MPV, eff)
    name_sensor=input("Enter the name of sensor 1: ")
    plot_charge(BV, MPV, name_sensor, args.Temperature, irradiation)
    plot_eff(BV,eff, name_sensor,args.Temperature, irradiation)
    #plt.axis([400, 600, 30, 70])

if __name__ == "__main__":
    main()
