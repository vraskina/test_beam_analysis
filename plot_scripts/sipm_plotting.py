import matplotlib.pyplot as plt
import matplotlib.markers as mrk
import os
import argparse
import fnmatch
from math import *
import numpy as np
from statistics import mean


def readthedata(file, res, reserr, error):
    with open(file) as f:
        lines = f.readlines()
        line = lines[2]
        a = line.split()[1:3]
        res.append(float(a[0]))
        reserr.append(2*float(a[1]))
        error.append(float(a[0]))

def scan_folder(path, res, reserr, error):
    # iterate over all the files in directory 'path'
    for file in os.listdir(path):
        if fnmatch.fnmatch(file,'batch*.txt'):
            fullname = os.path.join(path,file)
            print(fullname)
            readthedata(fullname, res, reserr, error)
        else:
            current_path = "".join((path, "/", file))
            if os.path.isdir(current_path):
                # if we're checking a sub-directory, recursively call this method
               scan_folder(current_path, res, reserr, error)
    return res, reserr, error

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--NBatch", nargs='+', help = "Show batch numbers")
    args = parser.parse_args()
    list_of_batches = args.NBatch
    fig = plt.figure()
    plt.xlabel('Batches', fontsize=15)
    plt.ylabel('Resolution [ps]', fontsize=15)
    error = []
    for batch in list_of_batches:
        res = []
        reserr = []
        scan_folder("../../Feb2022new/batch%s"%batch+"xx", res, reserr, error)
        print("Mean resolution for batch " + str(batch) + "xx: ", mean(res), "+-", mean(reserr))
        plt.errorbar([str(batch)+ "xx"]*len(res), res, yerr = reserr, marker = '.', markersize=8, linestyle='None')
    plt.ylim(55, 75)
    print(error)
    mean_error = mean(error)
    MSE = np.square(np.subtract(error,mean_error)).mean()
    print("mean: ", mean_error)
    print("sqrt MSE: ", sqrt(MSE))
    fig.suptitle('SiPM', fontsize=20)
    fig.savefig('SiPMreso.pdf')
    # plt.show()
    #plt.axis([400, 600, 30, 70])

if __name__ == "__main__":
    main()
