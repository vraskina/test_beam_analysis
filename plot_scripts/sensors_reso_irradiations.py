import matplotlib.pyplot as plt
import matplotlib.markers as mrk
import os
import argparse
import fnmatch
import numpy as np
from statistics import mean
import mplhep as hep
plt.style.use(hep.style.ROOT)

def readthedata(file, line, BV, res, reserr):
    with open(file) as f:
        lines = f.readlines()
        line_index = lines[int(line)]
        a = line_index.split()[0:3]
        BV.append(float(a[0]))
        res.append(float(a[1]))
        reserr.append(2*float(a[2]))

def readthedataDOUBLEerror(file, line, BV, res, reserr):
    with open(file) as f:
        lines = f.readlines()
        line_index = lines[int(line)]
        a = line_index.split()[0:3]
        BV.append(float(a[0]))
        res.append(float(a[1]))
        reserr.append(float(a[2]))

#Function works with MALTA data I didn;t analyse
def read_one_file(file):
    bv = []
    reso = []
    with open(file) as f:
        for line in f:
            a = line.split()[0:2]
            bv.append(float(a[0]))
            reso.append(float(a[1]))
    return bv, reso

def scan_folder(path, line, BV, res, reserr, double_err):
    # iterate over all the files in directory 'path'
    for file in os.listdir(path):
        if fnmatch.fnmatch(file,'batch*.txt'):
            fullname = os.path.join(path,file)
            print(fullname)
            print("double error: ", double_err)
            if double_err==True:
                readthedataDOUBLEerror(fullname, line, BV,  res, reserr)

            else:
                readthedata(fullname, line, BV,  res, reserr)
       
        else:
            current_path = "".join((path, "/", file))
            if os.path.isdir(current_path):
                # if we're checking a sub-directory, recursively call this method
               scan_folder(current_path, line, BV, res, reserr, double_err)
    return res, reserr

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--NBatch", nargs='+', help = "Show batch numbers")
    parser.add_argument("-l", "--NLines", nargs='+', help = "Show batch lines")
    parser.add_argument("-s", "--Nsensors", nargs='+', help = "List of sensors names")
    parser.add_argument("-t", "--Temperature", nargs='+', help = "Temperature")
    # parser.add_argument("-y", "--year", nargs='+', help = "Show the test beam campaign year")

    args = parser.parse_args()
    list_of_batches = args.NBatch
    lines = args.NLines
    irradiation = args.Nsensors
    TB = args.Temperature
    name = input("Enter the Sensor name ")
    #textstr = 'Irradiation = '+ str(irradiation) + ' $n_{eq}cm^{-2}$' # If you want sensors with the same irradiation
    textstr = str(name)  # If you want the sensor with different irradiations
    fig, ax = plt.subplots()
    plt.xlabel('Bias Voltage [V]')
    plt.ylabel('Resolution [ps]')
    colors = ['#00008B','#C20078','#15B01A', 'maroon' ,'#E50000']
    markers = ['o', 'd', '*', 'X']
    count = 0
    for batch in list_of_batches:
        BV = []
        res = []
        reserr = []
        double_err = False
        b_range = [3,7,8]
        if int(batch) in b_range:
            double_err = True
        scan_folder("../../Feb2022new/batch%s"%batch+"xx", lines[count], BV, res, reserr, double_err)
        ax.text(0.5, 0.45, textstr, transform=ax.transAxes)
        ax.errorbar(BV, res, yerr = reserr, marker = markers[count], markersize=12, linestyle='--', color = colors[count], label= "$\phi_{eq}$=" + "%s"%irradiation[count] + '$n_{eq}cm^{-2}$' + ", " + TB[count] + "$^{o}$C")
        count +=1
    #bv, reso = read_one_file("/eos/user/v/vraskina/Analysis_Test_Beam/Malta/IMEv2_reso.txt")
    #ax.errorbar(bv, reso, marker = markers[count], markersize=12, color = colors[count], linestyle='None', label=sensors_name[count] + " (" + TB[count]+" "+year[count]+")")
    legend = ax.legend()
    #hep.atlas.label(loc=0, data=False)
    hep.atlas.text(' HGTD Internal Test Beam')
    plt.ylim(30, 100)
    plt.xlim(80, 600)
    plt.hlines(70.0,80, 600,color='red',linestyle='dotted')
    fig.suptitle('', fontsize=20)
    fig.savefig('resolution_irradiations_%s'%name+'.pdf')
    plt.show()
    #plt.axis([400, 600, 30, 70])

if __name__ == "__main__":
    main()
