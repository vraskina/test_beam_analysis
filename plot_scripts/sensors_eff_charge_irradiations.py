import matplotlib.pyplot as plt
import matplotlib.markers as mrk
import os
import argparse
import fnmatch
import numpy as np
import mplhep as hep
plt.style.use(hep.style.ROOT)

def readthedata(file, BV, MPV, eff):
    with open(file) as f:
        #count = 0
        for line in f:
            a = line.split()[0:3]
            BV.append(float(a[0]))
            eff.append(float(a[1]))
            MPV.append(float(a[2]))
            #count += 1


def main():
    #plt.rcParams['text.usetex'] = True
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--File",nargs='+', help = "List of files to be used")
    parser.add_argument("-i", "--Iradiation", nargs='+', help = "List of sensors' iradiations")
    parser.add_argument("-t", "--temp", nargs='+', help = "Show the temperature")
    args = parser.parse_args()
    list_of_files = args.File
    irradiation = args.Iradiation
    temperature = args.temp
    sensor = input("Enter the sensor name")
    textstr = str(sensor)
    #textstr = str(irradiation)
    # colors = ['#00008B','#15B01A','#C20078', 'maroon' ,'#E50000']
    # #colors = ['tab:green','tab:purple','tab:red', 'tab:pink', '#E50000']
    # markers = ['o', 'd', '*', 'X']

    # colors = ['tab:green', '#0485d1', 'tab:red', 'tab:purple', 'tab:pink', ]
    # markers = ['o', 's',  '*', 'd', 'X']

    colors = ['#00008B','#C20078','#15B01A', 'maroon' ,'#E50000']
    markers = ['o', 'd', '*', 'X']

    fig_mpv, ax_mpv = plt.subplots()
    count = 0
    for file in list_of_files:
        BV = []
        MPV = []
        eff = []
        readthedata(file, BV, MPV, eff)
        ax_mpv.text(0.05, 0.05, textstr, transform=ax_mpv.transAxes)
        #plt.plot(BV,MPV, marker = '.', linestyle='None')
        plt.ylim(0,25)
        plt.xlim(80,600)
        ax_mpv.errorbar(BV,MPV, yerr = 0.03, marker = markers[count],  linestyle='--', markersize=12, color = colors[count], label= "$\phi_{eq}$=" + "%s"%irradiation[count] + '$n_{eq}cm^{-2}$' + ", " + temperature[count] + "$^{o}$C")
        #fig_mpv.suptitle(irradiation, fontsize=18)
        plt.xlabel('Bias Voltage[V]')
        plt.hlines(4.0,80,600,color='red',linestyle='dotted')
        plt.ylabel('Charge$_{\,\,MPV}$ [fC]')
        count+= 1
    hep.atlas.text(' Preliminary Test Beam')
    legend = ax_mpv.legend(loc = 'upper right',fontsize=16)
    #fig_mpv.savefig('MPV_irradiation_%s'%irradiation+'.png')
    fig_mpv.savefig('MPV_irradiations_%s'%sensor+'.pdf')

    plt.show()
    plt.clf()

    count = 0
    fig_eff, ax_eff = plt.subplots()
    for file in list_of_files:
        BV = []
        MPV = []
        eff = []
        readthedata(file, BV, MPV, eff)
        ax_eff.text(0.1, 0.1, textstr, transform=ax_eff.transAxes)
        #plt.plot(BV,eff, marker = '.', linestyle='None')
        ax_eff.errorbar(BV,eff, yerr = 0.1, marker = markers[count],  linestyle='--', markersize=12, color = colors[count], label= "$\phi_{eq}$=" + "%s"%irradiation[count] + '$n_{eq}cm^{-2}$' + ", " + temperature[count] + "$^{o}$C")
        plt.xlim(80,600)
        plt.ylim(85,110)
        #fig.suptitle('test title', fontsize=20)
        #fig_eff.suptitle(irradiation, fontsize=18)
        plt.xlabel('Bias Voltage[V]')
        plt.hlines(95.0,80,600,color='red',linestyle='dotted')
        plt.ylabel('Efficiency [%]')
        count+= 1
    hep.atlas.text(' Preliminary Test Beam')
    legend = ax_eff.legend(loc = 'upper right', fontsize=16)
    fig_eff.savefig('eff_irradiations_%s'%sensor+'.pdf')
    plt.show()
    plt.clf()
    #plt.axis([400, 600, 30, 70])

if __name__ == "__main__":
    main()
